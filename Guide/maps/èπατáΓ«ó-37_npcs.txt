  0: human       katana_npc@buusty_dialog.script:kur_spawn_questoviki:5474                             (Катана               )  @   61.8000,   -5.0600,   96.7700
  1: human       vanga_npc@buusty_dialog.script:kur_spawn_questoviki:5476                              (Ванга                )  @   72.3800,   -5.0600,  107.9000
  2: human       furii_sniper_1@buusty_dialog.script:kur_spawn_questoviki:5478                         (furii_sniper         )  @   77.9900,  -10.7100,  102.1300
  3: human       furii_sniper_2@buusty_dialog.script:kur_spawn_questoviki:5479                         (furii_sniper         )  @   77.2800,  -10.8800,  122.1300
  4: human       furii_sniper_3@buusty_dialog.script:kur_spawn_questoviki:5480                         (furii_sniper         )  @   57.1100,  -10.7000,  115.2500
  5: monster     kur_naim_attack1@buusty_dialog.script:kur_zashita_bazy:5502                           (sim_killer_veteran   )  @  -21.8300,  -11.1400,   35.5400
  6: monster     kur_naim_attack4@buusty_dialog.script:kur_zashita_bazy:5503                           (sim_killer_veteran   )  @   15.6900,  -11.1900,    4.8700
  7: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy:5504                           (sim_killer_master    )  @   89.9200,  -11.1900,  -27.0500
  8: monster     kur_naim_attack3@buusty_dialog.script:kur_zashita_bazy:5505                           (sim_killer_general   )  @  133.0400,  -11.1900,  -27.7200
  9: monster     kur_naim_attack10@buusty_dialog.script:kur_zashita_bazy:5506                          (sim_killer_veteran   )  @  175.1200,  -11.1900,   35.2200
 10: monster     kur_naim_attack5@buusty_dialog.script:kur_zashita_bazy:5507                           (sim_killer_master    )  @   -7.5300,  -11.2700,  136.7800
 11: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy:5508                           (sim_killer_veteran   )  @   76.3500,  -11.1900,  228.1200
 12: monster     kur_naim_attack4@buusty_dialog.script:kur_zashita_bazy1:5513                          (sim_killer_veteran   )  @   15.6900,  -11.1900,    4.8700
 13: monster     kur_naim_attack5@buusty_dialog.script:kur_zashita_bazy1:5514                          (sim_killer_master    )  @  201.3200,  -10.6200,   91.0900
 14: monster     kur_naim_attack6@buusty_dialog.script:kur_zashita_bazy1:5515                          (sim_killer_general   )  @  182.3000,  -11.1900,  151.8300
 15: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy1:5516                          (sim_killer_veteran   )  @  152.9200,  -11.1900,  226.8100
 16: monster     kur_naim_attack8@buusty_dialog.script:kur_zashita_bazy1:5517                          (sim_killer_master    )  @   54.5200,  -11.1900,  198.1800
 17: monster     kur_naim_attack9@buusty_dialog.script:kur_zashita_bazy1:5518                          (sim_killer_general   )  @  -12.2300,  -11.1900,  110.7600
 18: monster     kur_naim_attack1@buusty_dialog.script:kur_zashita_bazy1:5519                          (sim_killer_veteran   )  @   -8.6700,  -11.1900,   60.8800
 19: monster     kur_naim_attack1@buusty_dialog.script:kur_zashita_bazy2:5524                          (sim_killer_veteran   )  @   -8.6700,  -11.1900,   60.8800
 20: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy2:5525                          (sim_killer_master    )  @    4.2100,  -11.1900,   38.6000
 21: monster     kur_naim_attack3@buusty_dialog.script:kur_zashita_bazy2:5526                          (sim_killer_general   )  @    0.0200,  -11.1900,  -26.5500
 22: monster     kur_naim_attack4@buusty_dialog.script:kur_zashita_bazy2:5527                          (sim_killer_veteran   )  @   45.4000,  -11.1900,  -37.4500
 23: monster     kur_naim_attack5@buusty_dialog.script:kur_zashita_bazy2:5528                          (sim_killer_master    )  @   88.2000,  -11.1100,  -27.5700
 24: monster     kur_naim_attack9@buusty_dialog.script:kur_zashita_bazy2:5529                          (sim_killer_general   )  @  -12.2300,  -11.1900,  110.7600
 25: monster     kur_naim_attack10@buusty_dialog.script:kur_zashita_bazy2:5530                         (sim_killer_veteran   )  @   -7.5300,  -11.2700,  136.7800
 26: monster     kur_naim_attack1@buusty_dialog.script:kur_zashita_bazy3:5536                          (sim_killer_veteran   )  @  168.4100,  -11.1900,  -16.4200
 27: monster     kur_naim_attack5@buusty_dialog.script:kur_zashita_bazy3:5537                          (sim_killer_master    )  @   15.6900,  -11.1900,    4.8700
 28: monster     kur_naim_attack6@buusty_dialog.script:kur_zashita_bazy3:5538                          (sim_killer_general   )  @   89.9200,  -11.1900,  -27.0500
 29: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy3:5539                          (sim_killer_veteran   )  @  133.0400,  -11.1900,  -27.7200
 30: monster     kur_naim_attack8@buusty_dialog.script:kur_zashita_bazy3:5540                          (sim_killer_master    )  @  175.1200,  -11.1900,   35.2200
 31: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy3:5541                          (sim_killer_master    )  @  -21.8300,  -11.1400,   35.5400
 32: monster     kur_naim_attack4@buusty_dialog.script:kur_zashita_bazy3:5542                          (sim_killer_veteran   )  @   15.6900,  -11.1900,    4.8700
 33: monster     kur_naim_attack9@buusty_dialog.script:kur_zashita_bazy3:5543                          (sim_killer_general   )  @  143.5700,  -11.1900,  230.4100
 34: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy4:5549                          (sim_killer_veteran   )  @  168.4100,  -11.1900,  -16.4200
 35: monster     kur_naim_attack10@buusty_dialog.script:kur_zashita_bazy4:5550                         (sim_killer_veteran   )  @  201.3200,  -10.6200,   91.0900
 36: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy4:5551                          (sim_killer_master    )  @  182.3000,  -11.1900,  151.8300
 37: monster     kur_naim_attack3@buusty_dialog.script:kur_zashita_bazy4:5552                          (sim_killer_general   )  @  152.9200,  -11.1900,  226.8100
 38: monster     kur_naim_attack4@buusty_dialog.script:kur_zashita_bazy4:5553                          (sim_killer_veteran   )  @   54.5200,  -11.1900,  198.1800
 39: monster     kur_naim_attack9@buusty_dialog.script:kur_zashita_bazy4:5554                          (sim_killer_general   )  @  -12.2300,  -11.1900,  110.7600
 40: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy4:5555                          (sim_killer_master    )  @  167.1900,  -10.9600,   36.4100
 41: monster     kur_naim_attack10@buusty_dialog.script:kur_zashita_bazy5:5561                         (sim_killer_veteran   )  @   -8.6700,  -11.1900,   60.8800
 42: monster     kur_naim_attack6@buusty_dialog.script:kur_zashita_bazy5:5562                          (sim_killer_general   )  @    4.2100,  -11.1900,   38.6000
 43: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy5:5563                          (sim_killer_veteran   )  @    0.0200,  -11.1900,  -26.5500
 44: monster     kur_naim_attack8@buusty_dialog.script:kur_zashita_bazy5:5564                          (sim_killer_master    )  @   45.4000,  -11.1900,  -37.4500
 45: monster     kur_naim_attack5@buusty_dialog.script:kur_zashita_bazy5:5565                          (sim_killer_master    )  @   88.2000,  -11.1100,  -27.5700
 46: monster     kur_naim_attack5@buusty_dialog.script:kur_zashita_bazy5:5566                          (sim_killer_master    )  @   15.6900,  -11.1900,    4.8700
 47: monster     kur_naim_attack6@buusty_dialog.script:kur_zashita_bazy5:5567                          (sim_killer_general   )  @   89.9200,  -11.1900,  -27.0500
 48: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy5:5568                          (sim_killer_veteran   )  @  133.0400,  -11.1900,  -27.7200
 49: monster     kur_naim_attack8@buusty_dialog.script:kur_zashita_bazy5:5569                          (sim_killer_master    )  @  175.1200,  -11.1900,   35.2200
 50: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy5:5570                          (sim_killer_master    )  @  -21.8300,  -11.1400,   35.5400
 51: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy6:5576                          (sim_killer_master    )  @  168.4100,  -11.1900,  -16.4200
 52: monster     kur_naim_attack3@buusty_dialog.script:kur_zashita_bazy6:5577                          (sim_killer_general   )  @   15.6900,  -11.1900,    4.8700
 53: monster     kur_naim_attack4@buusty_dialog.script:kur_zashita_bazy6:5578                          (sim_killer_veteran   )  @   89.9200,  -11.1900,  -27.0500
 54: monster     kur_naim_attack5@buusty_dialog.script:kur_zashita_bazy6:5579                          (sim_killer_master    )  @  133.0400,  -11.1900,  -27.7200
 55: monster     kur_naim_attack9@buusty_dialog.script:kur_zashita_bazy6:5580                          (sim_killer_general   )  @  175.1200,  -11.1900,   35.2200
 56: monster     kur_naim_attack10@buusty_dialog.script:kur_zashita_bazy6:5581                         (sim_killer_veteran   )  @  201.3200,  -10.6200,   91.0900
 57: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy6:5582                          (sim_killer_veteran   )  @  182.3000,  -11.1900,  151.8300
 58: monster     kur_naim_attack7@buusty_dialog.script:kur_zashita_bazy7:5587                          (sim_killer_veteran   )  @   76.3500,  -11.1900,  228.1200
 59: monster     kur_naim_attack10@buusty_dialog.script:kur_zashita_bazy7:5588                         (sim_killer_veteran   )  @  143.5700,  -11.1900,  230.4100
 60: monster     kur_naim_attack2@buusty_dialog.script:kur_zashita_bazy7:5589                          (sim_killer_master    )  @  182.3000,  -11.1900,  151.8300
 61: monster     kur_naim_attack3@buusty_dialog.script:kur_zashita_bazy7:5590                          (sim_killer_general   )  @  152.9200,  -11.1900,  226.8100
 62: monster     kur_naim_attack4@buusty_dialog.script:kur_zashita_bazy7:5591                          (sim_killer_veteran   )  @   54.5200,  -11.1900,  198.1800
 63: monster     kur_naim_attack9@buusty_dialog.script:kur_zashita_bazy7:5592                          (sim_killer_general   )  @  -12.2300,  -11.1900,  110.7600
 64: monster     kur_naim_attack11@buusty_dialog.script:kur_zashita_bazy8:5598                         (GENERATE_NAME_stalker)  @  168.4100,  -11.1900,  -16.4200
 65: monster     kur_naim_attack12@buusty_dialog.script:kur_zashita_bazy8:5599                         (GENERATE_NAME_stalker)  @   15.6900,  -11.1900,    4.8700
 66: monster     kur_naim_attack13@buusty_dialog.script:kur_zashita_bazy8:5600                         (GENERATE_NAME_stalker)  @  -12.2300,  -11.1900,  110.7600
 67: monster     kur_naim_attack14@buusty_dialog.script:kur_zashita_bazy8:5601                         (GENERATE_NAME_stalker)  @    4.2100,  -11.1900,   38.6000
 68: monster     kur_naim_attack15@buusty_dialog.script:kur_zashita_bazy8:5602                         (GENERATE_NAME_stalker)  @   89.9200,  -11.1900,  -27.0500
 69: monster     naemnik_sniper@new_gravikostum.script:new_gravikostum_start:13                        (GENERATE_NAME_stalker)  @  140.8240,   -4.3160, -195.3620
 70: monster     naemnik_regular@new_gravikostum.script:new_gravikostum_start:14                       (sim_killer_general   )  @  171.6320,  -11.2900, -210.2780
 71: monster     naemnik_veteran@new_gravikostum.script:new_gravikostum_start:15                       (sim_killer_veteran   )  @  165.5920,  -11.2950, -179.3300
 72: monster     pri_arhara_naemnik1@new_gravikostum.script:new_gravikostum_start:16                   (sim_killer_master    )  @  133.7070,   -4.3140, -195.3210
 73: monster     pri_arhara_naemnik2@new_gravikostum.script:new_gravikostum_start:17                   (GENERATE_NAME_stalker)  @  118.2570,   -4.3140, -197.0890
 74: monster     pri_arhara_naemnik3@new_gravikostum.script:new_gravikostum_start:18                   (GENERATE_NAME_stalker)  @  143.8510,   -9.8140, -196.3740
 75: monster     naemnik_regular@new_gravikostum.script:new_gravikostum_start:19                       (sim_killer_general   )  @  108.3900,  -11.2940, -180.0940
 76: monster     naemnik_veteran@new_gravikostum.script:new_gravikostum_start:20                       (sim_killer_veteran   )  @  184.9080,  -11.2890, -183.1160
 77: monster     zaton_vermaht5@new_gravikostum.script:new_gravikostum_start:21                        (GENERATE_NAME_vermaht)  @  205.2620,  -11.1890, -157.3970
 78: monster     zaton_vermaht6@new_gravikostum.script:new_gravikostum_start:22                        (GENERATE_NAME_vermaht)  @  166.5450,  -11.1340, -157.4440
 79: monster     zaton_vermaht7@new_gravikostum.script:new_gravikostum_start:23                        (GENERATE_NAME_vermaht)  @  158.1700,  -11.1900, -158.1370
 80: monster     zaton_vermaht5@new_gravikostum.script:new_gravikostum_start:24                        (GENERATE_NAME_vermaht)  @  137.9790,  -11.2170, -166.1990
 81: monster     zaton_vermaht6@new_gravikostum.script:new_gravikostum_start:25                        (GENERATE_NAME_vermaht)  @  116.9830,  -11.1400, -161.0900
 82: monster     zaton_vermaht7@new_gravikostum.script:new_gravikostum_start:26                        (GENERATE_NAME_vermaht)  @  108.0810,  -11.1890, -159.1920
 83: car/turret  auto_turret_2@new_gravikostum.script:new_gravikostum_start:27                         (                     )  @  158.8530,    9.8860, -198.2100
