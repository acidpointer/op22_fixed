  0: item        Criminal note 19                                                   note_zone_19             ("НС - Генераторы"          ) @  26.4779,  -3.1663,   1.3146
  1: manual item diary_chess_6@oksana_spawn.script:general_note:678                 diary_chess_6            (Часть Дневника Шахматиста  ) @ -42.1320,   3.1910,  -7.6100
  2: manual item note_15@oksana_spawn.script:sudden_ambush_3:1524                   note_15                  (Записка Архары             ) @  11.8220,  10.2590, -71.5820
  3: manual item note_16@oksana_spawn.script:sudden_ambush_3:1525                   note_16                  (Шахматный ход              ) @ -56.3100,   0.7910,  16.8100
  4: manual item af_rusty_sea-urchin1@oksana_spawn.script:sudden_ambush_3:1526      af_rusty_sea-urchin1     ("Морской ёж"               ) @ -51.2140,   1.7930,  25.4460
  5: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1527       af_rusty_sea-urchin      ("Морской ёж"               ) @ -89.0480,   3.5350,   6.3750
  6: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1528       af_rusty_sea-urchin      ("Морской ёж"               ) @ -54.0960,   2.4270,  -2.3610
  7: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1529       af_rusty_sea-urchin      ("Морской ёж"               ) @ -53.9070,   2.6050, -10.9760
  8: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1530       af_rusty_sea-urchin      ("Морской ёж"               ) @ -68.6200,   3.2300, -17.6710
  9: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1531       af_rusty_sea-urchin      ("Морской ёж"               ) @ -45.4320,   2.8007, -29.2630
 10: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1532       af_rusty_sea-urchin      ("Морской ёж"               ) @ -57.7220,   2.3230, -27.6590
 11: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1533       af_rusty_sea-urchin      ("Морской ёж"               ) @ -60.5670,   2.6930, -19.1400
 12: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1534       af_rusty_sea-urchin      ("Морской ёж"               ) @ -80.3730,   2.6260, -14.8660
 13: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1535       af_rusty_sea-urchin      ("Морской ёж"               ) @ -69.4150,   2.0110,  12.9980
 14: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1536       af_rusty_sea-urchin      ("Морской ёж"               ) @ -58.0790,   1.6290,   8.8570
 15: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1537       af_rusty_sea-urchin      ("Морской ёж"               ) @ -73.5870,   2.6700, -33.4500
 16: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1538       af_rusty_sea-urchin      ("Морской ёж"               ) @ -63.0050,   2.5370, -33.9750
 17: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1539       af_rusty_sea-urchin      ("Морской ёж"               ) @ -49.6590,   2.6880, -31.0180
 18: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1540       af_rusty_sea-urchin      ("Морской ёж"               ) @ -68.4510,   3.2300,  -9.0280
 19: manual item af_rusty_sea-urchin@oksana_spawn.script:sudden_ambush_3:1541       af_rusty_sea-urchin      ("Морской ёж"               ) @ -73.1770,   2.6950,  -3.3760
 20: manual item carpet_striped@oksana_spawn.script:consumer_society:4633           carpet_striped           (Полосатый Ковёр            ) @  78.3470,   4.5450, -63.5200
 21: manual item green_carpet@oksana_spawn.script:consumer_society:4634             green_carpet             (Зелёный Ковёр              ) @  82.0130,  10.1620,  27.4950
 22: manual item snp_pda4@snp.script:fenrir7_done:787                               snp_pda4                 (КПК снайпера               ) @  89.4518,   8.0236, -36.2717
 23: manual item snp_plan@snp.script:bubulyka2_done:1022                            snp_plan                 (Мешок с "травой"           ) @ 127.4698,  15.7251, -85.3887
 24: item        village_11.43x23_hydro1                                            ammo_11.43x23_hydro      (Патроны .45 ACP Hydra-Shock) @  11.8048,  10.2608, -70.3482
 25: item        village_11.43x23_hydro2                                            ammo_11.43x23_hydro      (Патроны .45 ACP Hydra-Shock) @  11.8048,  10.2608, -70.3482
 26: item        lost_village_conserva                                              conserva                 (Conserva                   ) @  88.3721,   8.3251, -36.3872
 27: item        lost_village_conserva_0000                                         conserva                 (Conserva                   ) @  88.0989,   8.3253, -36.3866
 28: LevelWarper lost_village_hospital_shape_0                                                                                             @  48.9985,   7.0090, 100.7593 1 x 1
 29: LevelWarper lost_village_limansk_shape_0                                                                                              @ -86.9056,  10.5732, -83.4156 4.10285997390747 x 7.41159772872925
 30: item        lost_village1_vodka                                                vodka                    (Vodka                      ) @  88.1114,   8.3253, -36.8618
 31: item        village_vodka1                                                     vodka                    (Vodka                      ) @   4.4545,  11.6979, -33.8782
 32: item        village_vodka2                                                     vodka                    (Vodka                      ) @   4.4545,  11.6979, -33.8782
 33: item        village_vodka3                                                     vodka                    (Vodka                      ) @   4.4545,  11.6979, -33.8782
 34: item        village_vodka4                                                     vodka                    (Vodka                      ) @  11.8080,  10.2604, -71.5804
 35: item        village_vodka5                                                     vodka                    (Vodka                      ) @  16.2144,  14.1979,  24.7918
