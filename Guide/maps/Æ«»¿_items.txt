  0: item        swamp_ammo_11.43x23_fmj                     ammo_11.43x23_fmj        (Патроны .45 ACP                        ) @  -37.9678,    7.7984,  273.4700
  1: item        swamp_ammo_11.43x23_fmj_0000                ammo_11.43x23_fmj        (Патроны .45 ACP                        ) @  -37.9513,    7.7984,  273.3045
  2: item        swamp_ammo_11.43x23_fmj_0001                ammo_11.43x23_fmj        (Патроны .45 ACP                        ) @  -37.9586,    7.7985,  273.1440
  3: item        swamp_ammo_5.45x39_ap                       ammo_5.45x39_ap          (Патроны 5.45x39мм БП                   ) @  -44.9891,    7.3661,  275.9224
  4: item        swamp_ammo_5.45x39_ap_0000                  ammo_5.45x39_ap          (Патроны 5.45x39мм БП                   ) @  -45.0726,    7.3663,  276.1658
  5: item        swamp_ammo_5.45x39_ap_0001                  ammo_5.45x39_ap          (Патроны 5.45x39мм БП                   ) @  101.9616,    3.5846,   92.0040
  6: item        swamp_ammo_5.45x39_ap_0002                  ammo_5.45x39_ap          (Патроны 5.45x39мм БП                   ) @  102.2552,    3.5158,   92.1595
  7: item        swamp_ammo_5.45x39_fmj                      ammo_5.45x39_fmj         (Патроны 5.45x39мм                      ) @  -44.9945,    7.7707,  276.1791
  8: item        swamp_ammo_5.45x39_fmj_0000                 ammo_5.45x39_fmj         (Патроны 5.45x39мм                      ) @  -44.9873,    7.7707,  275.9490
  9: item        swamp_ammo_5.45x39_fmj_0001                 ammo_5.45x39_fmj         (Патроны 5.45x39мм                      ) @  -45.0358,    7.7705,  275.1436
 10: item        swamp_ammo_5.45x39_fmj_0002                 ammo_5.45x39_fmj         (Патроны 5.45x39мм                      ) @  -45.0349,    7.7706,  275.3874
 11: item        swamp_ammo_5.56x45_ap                       ammo_5.56x45_ap          (Патроны 5.56x45мм AP                   ) @  -37.9334,    7.0027,  274.1714
 12: item        swamp_ammo_5.56x45_ap_0000                  ammo_5.56x45_ap          (Патроны 5.56x45мм AP                   ) @  -37.9508,    7.0027,  273.9648
 13: item        swamp_ammo_5.56x45_ap_0001                  ammo_5.56x45_ap          (Патроны 5.56x45мм AP                   ) @  -37.9513,    7.0027,  273.7880
 14: item        swamp_ammo_7.62x54_7h1                      ammo_7.62x54_7h1         (Патроны 7.62x54мм 7H1                  ) @  -37.9461,    7.3958,  273.1468
 15: item        swamp_ammo_7.62x54_7h1_0000                 ammo_7.62x54_7h1         (Патроны 7.62x54мм 7H1                  ) @  -37.9540,    7.3958,  273.3640
 16: item        swamp_ammo_7.62x54_7h1_0001                 ammo_7.62x54_7h1         (Патроны 7.62x54мм 7H1                  ) @  -37.9445,    7.3958,  273.5675
 17: item        swamp_ammo_9x18_fmj                         ammo_9x18_fmj            (Патроны 9x18мм                         ) @  -37.9199,    7.0029,  273.1372
 18: item        swamp_ammo_9x18_fmj_0000                    ammo_9x18_fmj            (Патроны 9x18мм                         ) @  -37.9104,    7.0028,  273.2846
 19: item        swamp_ammo_9x18_fmj_0001                    ammo_9x18_fmj            (Патроны 9x18мм                         ) @  -37.9371,    7.0028,  273.4167
 20: item        swamp_ammo_9x19_fmj                         ammo_9x19_fmj            (Патроны 9x19мм Парабеллум              ) @  -37.8777,    7.3957,  274.1910
 21: item        swamp_ammo_9x19_fmj_0000                    ammo_9x19_fmj            (Патроны 9x19мм Парабеллум              ) @  -37.8886,    7.3957,  274.0490
 22: item        swamp_ammo_9x19_fmj_0001                    ammo_9x19_fmj            (Патроны 9x19мм Парабеллум              ) @  -37.8929,    7.3957,  273.9286
 23: item        swamp_ammo_9x19_fmj_0002                    ammo_9x19_fmj            (Патроны 9x19мм Парабеллум              ) @  -37.8808,    7.3957,  273.7837
 24: item        swamp_ammo_9x39_sp5                         ammo_9x39_sp5            (Патроны 9x39мм СП-5                    ) @  -37.9452,    7.7983,  274.1463
 25: item        swamp_ammo_9x39_sp5_0000                    ammo_9x39_sp5            (Патроны 9x39мм СП-5                    ) @  -37.9604,    7.7983,  273.9081
 26: item        swamp_ammo_9x39_sp5_0001                    ammo_9x39_sp5            (Патроны 9x39мм СП-5                    ) @  -37.9544,    7.7984,  273.7109
 27: item        swamp_ammo_og-7b                            ammo_og-7b               (ПГ-7ВЛ «Луч»                           ) @  -37.7621,    6.3432,  273.6684
 28: item        swamp_ammo_og-7b_0000                       ammo_og-7b               (ПГ-7ВЛ «Луч»                           ) @  -38.0221,    6.3432,  273.6684
 29: item        swamp_antirad                               antirad                  (Antirad                                ) @  -39.7749,    7.6923,  266.1046
 30: item        swamp_antirad_0000                          antirad                  (Antirad                                ) @  -39.9091,    7.6923,  266.1224
 31: item        swamp_antirad_0001                          antirad                  (Antirad                                ) @  -39.8174,    7.6923,  266.2622
 32: item        swamp_antirad_0002                          antirad                  (Antirad                                ) @  -35.0030,    3.7208,  -24.9626
 33: item        swamp_antirad_0003                          antirad                  (Antirad                                ) @  -35.0967,    3.7141,  -25.1138
 34: item        swamp_conserva                              conserva                 (Conserva                               ) @  -40.9592,    7.6923,  266.8184
 35: item        swamp_conserva_0000                         conserva                 (Conserva                               ) @  -40.7290,    7.6923,  266.9169
 36: item        swamp_conserva_0001                         conserva                 (Conserva                               ) @  -40.8010,    7.6923,  266.5971
 37: item        swamp_conserva_0002                         conserva                 (Conserva                               ) @   57.7394,    5.4793, -244.0461
 38: item        swamp_conserva_0003                         conserva                 (Conserva                               ) @  -34.8167,    3.7084,  -24.9644
 39: item        swamp_energy_drink                          energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -41.3027,    7.6923,  266.4700
 40: item        swamp_energy_drink_0000                     energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -41.1954,    7.6923,  266.2232
 41: item        swamp_energy_drink_0001                     energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -41.1819,    7.6923,  266.4687
 42: item        swamp_energy_drink_0002                     energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -87.3269,    3.1503,  155.8640
 43: item        swamp_energy_drink_0003                     energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -87.2553,    3.1503,  155.9725
 44: item        swamp_energy_drink_0004                     energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -87.3447,    3.1503,  155.9606
 45: item        swamp_killer_outfit                         killer_outfit            (Комбинезон наёмника                    ) @ -283.3703,    1.7795,  127.4347
 46: item        swamp_medkit                                medkit                   (Medkit                                 ) @   88.9502,    7.1060,  255.6543
 47: item        swamp_medkit_scientic                       medkit_scientic          (Medkit_scientific                      ) @  -40.1177,    7.6923,  266.2384
 48: item        swamp_medkit_scientic_0000                  medkit_scientic          (Medkit_scientific                      ) @  -40.1284,    7.6923,  266.5359
 49: item        swamp_medkit_scientic_0001                  medkit_scientic          (Medkit_scientific                      ) @  -40.3788,    7.6923,  266.5068
 50: item        swamp_medkit_scientic_0002                  medkit_scientic          (Medkit_scientific                      ) @  -40.3704,    7.6923,  266.2148
 51: item        swamp_vodka                                 vodka                    (Vodka                                  ) @  131.8709,    6.9585,  253.1063
 52: item        swamp_vodka_0000                            vodka                    (Vodka                                  ) @  132.0264,    6.9585,  253.2056
 53: item        swamp_vodka_0001                            vodka                    (Vodka                                  ) @  131.8651,    6.9585,  253.2803
 54: item        swamp_vodka_0002                            vodka                    (Vodka                                  ) @  -69.7884,    6.7737,  249.5026
 55: item        swamp_vodka_0003                            vodka                    (Vodka                                  ) @  -87.3262,    3.1503,  156.1139
 56: item        swamp_vodka_0004                            vodka                    (Vodka                                  ) @  -87.3943,    3.1503,  156.1945
 57: item        swamp_vodka_0005                            vodka                    (Vodka                                  ) @ -283.3380,    2.0547,  128.3200
 58: item        swamp_vodka_0006                            vodka                    (Vodka                                  ) @ -283.3102,    2.0547,  128.1536
 59: item        swamp_vodka_0007                            vodka                    (Vodka                                  ) @ -283.3803,    2.0547,  128.2424
