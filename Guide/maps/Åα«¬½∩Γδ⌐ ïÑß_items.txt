  0: item        mr_explosive_mobiltank                   amk_kanistra             (Канистра с бензином                    ) @   212.0015,   -15.2242,    20.6694
  1: item        mr_explosive_mobiltank_0000              amk_kanistra             (Канистра с бензином                    ) @  -131.9086,     5.2433,   -85.8480
  2: item        mr_explosive_mobiltank_0001              amk_kanistra             (Канистра с бензином                    ) @  -188.2751,     8.6911,   -93.9169
  3: item        mr_explosive_mobiltank_0002              amk_kanistra             (Канистра с бензином                    ) @  -188.5709,     8.6911,   -93.9249
  4: item        mr_ammo_12x70_buck                       ammo_12x70_buck          (Патроны 12x70 Дробь                    ) @   177.9292,   -15.0847,    17.7875
  5: item        mr_ammo_12x70_buck_0000                  ammo_12x70_buck          (Патроны 12x70 Дробь                    ) @   177.9514,   -15.0847,    17.4591
  6: item        mr_ammo_12x70_buck_0001                  ammo_12x70_buck          (Патроны 12x70 Дробь                    ) @   210.1632,   -14.6694,    21.2269
  7: item        mr_ammo_12x70_buck_0002                  ammo_12x70_buck          (Патроны 12x70 Дробь                    ) @   210.5319,   -14.6694,    21.1900
  8: item        mr_ammo_12x76_dart                       ammo_12x76_dart          (Патроны 12x76 Дротик                   ) @   151.7024,   -15.0769,    50.2708
  9: item        mr_ammo_12x76_dart_0000                  ammo_12x76_dart          (Патроны 12x76 Дротик                   ) @   151.7044,   -15.0769,    50.4069
 10: item        mr_ammo_12x76_dart_0001                  ammo_12x76_dart          (Патроны 12x76 Дротик                   ) @   151.7066,   -15.0769,    50.5391
 11: item        mr_ammo_12x76_dart_0002                  ammo_12x76_dart          (Патроны 12x76 Дротик                   ) @  -220.0877,     8.7750,  -104.9265
 12: item        mr_ammo_12x76_dart_0003                  ammo_12x76_dart          (Патроны 12x76 Дротик                   ) @  -220.0903,     8.7750,  -105.0713
 13: item        mr_ammo_12x76_zhekan                     ammo_12x76_zhekan        (Патроны 12x76 Жекан                    ) @   186.6297,   -14.5327,   711.9926
 14: item        mr_ammo_12x76_zhekan_0000                ammo_12x76_zhekan        (Патроны 12x76 Жекан                    ) @   186.9523,   -14.5311,   711.9692
 15: item        mr_ammo_5.45x39_ap                       ammo_5.45x39_ap          (Патроны 5.45x39мм БП                   ) @   249.0777,   -15.6403,    31.4019
 16: item        mr_ammo_5.45x39_ap_0000                  ammo_5.45x39_ap          (Патроны 5.45x39мм БП                   ) @   249.4033,   -15.6403,    31.4021
 17: item        mr_ammo_5.45x39_fmj                      ammo_5.45x39_fmj         (Патроны 5.45x39мм                      ) @   156.5664,   -15.8180,    51.2132
 18: item        mr_ammo_5.45x39_fmj_0000                 ammo_5.45x39_fmj         (Патроны 5.45x39мм                      ) @  -372.2010,     3.1155,  -114.5852
 19: item        mr_ammo_5.45x39_fmj_0001                 ammo_5.45x39_fmj         (Патроны 5.45x39мм                      ) @  -372.2677,     3.1155,  -114.1440
 20: item        mr_antirad                               antirad                  (Antirad                                ) @   161.2591,   -15.2860,   704.9906
 21: item        mr_antirad_0000                          antirad                  (Antirad                                ) @   161.3923,   -15.3014,   704.9634
 22: item        mr_antirad_0001                          antirad                  (Antirad                                ) @   155.5996,   -15.2957,    46.4747
 23: item        mr_antirad_0002                          antirad                  (Antirad                                ) @   155.6813,   -15.2957,    46.3456
 24: item        mr_antirad_0003                          antirad                  (Antirad                                ) @  -212.0343,     8.8937,  -129.4912
 25: item        mr_antirad_0004                          antirad                  (Antirad                                ) @  -212.1206,     8.8938,  -129.3925
 26: item        mr_antirad_0005                          antirad                  (Antirad                                ) @  -212.2241,     8.8938,  -129.4501
 27: item        mr_antirad_0006                          antirad                  (Antirad                                ) @  -368.1592,     4.9155,  -112.1612
 28: item        mr_antirad_0007                          antirad                  (Antirad                                ) @  -368.3665,     4.9155,  -112.1331
 29: item        mr_antirad_0008                          antirad                  (Antirad                                ) @  -368.2563,     4.9155,  -112.4242
 30: item        mr_bread                                 bread                    (Bread                                  ) @  -220.0148,     9.0662,  -105.2807
 31: item        mr_conserva                              conserva                 (Conserva                               ) @  -242.1731,    12.0567,  -138.1609
 32: item        mr_conserva_0000                         conserva                 (Conserva                               ) @  -242.2913,    12.0568,  -138.3029
 33: item        mr_conserva_0001                         conserva                 (Conserva                               ) @  -327.8364,    -4.3844,   916.6664
 34: item        mr_conserva_0002                         conserva                 (Conserva                               ) @   172.9519,   -13.7998,   730.1460
 35: item        mr_conserva_0003                         conserva                 (Conserva                               ) @   138.7911,   -15.1154,   732.1851
 36: item        mr_conserva_0004                         conserva                 (Conserva                               ) @   138.8476,   -15.1154,   731.9993
 37: item        mr_detector_simple                       detector_simple          (Дозиметр                               ) @   153.1233,   -15.8180,    52.0197
 38: item        mr_energy_drink                          energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -247.5819,     9.5399,  -137.5396
 39: item        mr_energy_drink_0000                     energy_drink             (Энергетический напиток "S.T.A.L.K.E.R.") @  -331.9816,    -3.9080,   916.7748
 40: item        mr_medkit                                medkit                   (Medkit                                 ) @  -222.0179,     8.8058,  -101.1414
 41: item        mr_medkit_0000                           medkit                   (Medkit                                 ) @  -220.0885,     9.0662,  -104.9641
 42: item        mr_medkit_army                           medkit_army              (Medkit_army                            ) @  -322.6943,    -3.4606,   916.9354
 43: item        mr_medkit_army_0000                      medkit_army              (Medkit_army                            ) @  -330.2307,    -4.3811,   912.7375
 44: item        mr_vodka                                 vodka                    (Vodka                                  ) @  -332.4619,    -4.8301,   915.9052
 45: item        mr_vodka_0000                            vodka                    (Vodka                                  ) @  -332.3296,    -4.8301,   915.9435
 46: item        mr_vodka_0001                            vodka                    (Vodka                                  ) @  -332.3843,    -4.8301,   915.8179
 47: item        mr_vodka_0002                            vodka                    (Vodka                                  ) @   130.8811,   -16.1043,   701.0320
 48: item        mr_vodka_0003                            vodka                    (Vodka                                  ) @   130.9749,   -16.1053,   701.0597
 49: item        mr_vodka_0004                            vodka                    (Vodka                                  ) @   131.0605,   -16.1053,   701.0104
 50: item        mr_vodka_0005                            vodka                    (Vodka                                  ) @   247.9137,   -11.6237,    33.2934
 51: item        mr_vodka_0006                            vodka                    (Vodka                                  ) @   147.7483,   -15.8180,    47.2183
 52: item        mr_vodka_0007                            vodka                    (Vodka                                  ) @   147.6094,   -15.8180,    47.1246
 53: item        mr_vodka_0008                            vodka                    (Vodka                                  ) @   147.6202,   -15.8180,    47.3365
 54: item        mr_vodka_0009                            vodka                    (Vodka                                  ) @   252.8671,   -15.6402,    35.6520
 55: item        mr_vodka_0010                            vodka                    (Vodka                                  ) @   252.9108,   -15.6402,    35.7335
 56: item        mr_vodka_0011                            vodka                    (Vodka                                  ) @  -219.9097,     9.3687,  -105.4236
 57: item        mr_vodka_0012                            vodka                    (Vodka                                  ) @  -219.8682,     9.3687,  -105.2340
 58: item        mr_vodka_0013                            vodka                    (Vodka                                  ) @  -220.0451,     9.3687,  -105.3145
 59: item        mr_vodka_0014                            vodka                    (Vodka                                  ) @  -206.4139,     8.8940,  -130.7756
 60: item        mr_vodka_0015                            vodka                    (Vodka                                  ) @  -206.6505,     8.8940,  -130.8254
 61: item        mr_vodka_0016                            vodka                    (Vodka                                  ) @  -206.6115,     8.8940,  -130.7200
