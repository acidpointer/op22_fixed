  0: LevelWarper level_changer                                                                                                                   @  -67.3700,   26.0000,  567.1800 1 x 1
  1: manual item ammo_23x76_shrap@new_spawn.script:spawn_bonus_tayniki:1229            ammo_23x76_shrap         (Патроны 23x76 «Шрапнель 25»   ) @  -43.3900,   27.1300,  538.1000
  2: manual item amk_kanistra@dolg_btr.script:dolg_btr_done:35                         amk_kanistra             (Канистра с бензином           ) @  -23.8520,   33.1070,  370.8530
  3: manual item car_akb r0@dolg_btr.script:dolg_btr_done:37                           car_akb                  (Автомобильный аккумулятор     ) @  -39.8410,   25.8310,  384.9270
  4: manual item car_akb r1@dolg_btr.script:dolg_btr_done:37                           car_akb                  (Автомобильный аккумулятор     ) @  -32.4920,   25.8680,  387.4600
  5: manual item car_akb r2@dolg_btr.script:dolg_btr_done:37                           car_akb                  (Автомобильный аккумулятор     ) @  -17.0860,   34.3260,  308.0560
  6: manual item car_akb r3@dolg_btr.script:dolg_btr_done:37                           car_akb                  (Автомобильный аккумулятор     ) @  -20.9850,   34.3080,  296.1510
  7: manual item car_akb r4@dolg_btr.script:dolg_btr_done:37                           car_akb                  (Автомобильный аккумулятор     ) @  -20.2310,   34.4110,  285.5140
  8: manual item car_akb r5@dolg_btr.script:dolg_btr_done:37                           car_akb                  (Автомобильный аккумулятор     ) @  -22.7910,   34.4120,  273.3120
  9: item        dam_ammo_5.56x45_ss190                                                ammo_5.56x45_ss190       (Патроны 5.56x45мм SS109       ) @  -19.0336,   33.4699,  423.1859
 10: item        dam_ammo_5.56x45_ss190_0000                                           ammo_5.56x45_ss190       (Патроны 5.56x45мм SS109       ) @  -18.9046,   33.4700,  422.9798
 11: item        dam_ammo_5.56x45_ss190_0001                                           ammo_5.56x45_ss190       (Патроны 5.56x45мм SS109       ) @  -18.8401,   33.4699,  422.7714
 12: item        dam_ammo_7.62x54_7h14                                                 ammo_7.62x54_7h14        (Патроны 7.62x54мм 7Н14 СН     ) @   26.0260,   33.7536,  391.2694
 13: item        dam_ammo_7.62x54_7h14_0000                                            ammo_7.62x54_7h14        (Патроны 7.62x54мм 7Н14 СН     ) @   26.0493,   33.7536,  391.4659
 14: item        dam_antirad                                                           antirad                  (Antirad                       ) @  -71.9806,   38.6373,  642.7179
 15: item        dam_antirad_0000                                                      antirad                  (Antirad                       ) @  -71.8340,   38.6373,  642.8694
 16: item        dam_antirad_0001                                                      antirad                  (Antirad                       ) @  -28.4416,   25.1105,  372.4416
 17: item        dam_antirad_0002                                                      antirad                  (Antirad                       ) @  -44.9363,   29.3484,  322.6748
 18: item        dam_antirad_0003                                                      antirad                  (Antirad                       ) @  -23.6244,   41.8845,  343.0515
 19: item        dam_antirad_0004                                                      antirad                  (Antirad                       ) @   -4.5470,   33.6627,  307.7589
 20: item        dam_antirad_0005                                                      antirad                  (Antirad                       ) @   -4.4053,   33.6627,  307.7941
 21: item        dam_antirad_0006                                                      antirad                  (Antirad                       ) @   -4.3113,   33.6627,  307.6544
 22: item        dam_dolg_scientific_outfit                                            dolg_scientific_outfit   (ПСЗ-9МД «Универсальная защита») @  -29.4136,    3.7154,  121.8424
 23: item        dam_medkit_scientic                                                   medkit_scientic          (Medkit_scientific             ) @  -28.5467,   25.7701,  371.2837
 24: item        dam_medkit_scientic_0000                                              medkit_scientic          (Medkit_scientific             ) @  -44.7452,   29.3484,  322.9146
 25: item        dam_medkit_scientic_0001                                              medkit_scientic          (Medkit_scientific             ) @  -62.7738,   27.6537,  274.0505
 26: item        dam_monolit_outfit                                                    monolit_outfit           (Комбинезон Монолита           ) @   -6.0146,   33.5684,  307.8524
 27: item        dam_vodka                                                             vodka                    (Vodka                         ) @  -45.3595,   29.3484,  323.4093
