  0: monster     controller_filin@arhara_dialog.script:spawn_trupak:1744                                                     (Контролёр               )  @   71.5660,    0.9872,  338.9222
  1: monster     girl_zombied1@arhara_dialog.script:spawn_trupak:1745                                                        (GENERATE_NAME_girls     )  @   77.3265,    0.8061,  328.8094
  2: monster     voen_zombied2@arhara_dialog.script:spawn_trupak:1746                                                        (pri_zombied_veteran     )  @   50.6220,    0.1979,  349.3722
  3: monster     zombie_blow@arhara_dialog.script:spawn_trupak:1747                                                          (Зомби-камикадзе         )  @   24.6278,    0.5388,  337.6839
  4: monster     zombie_beee@arhara_dialog.script:spawn_trupak:1748                                                          (Взрывающийся зомби      )  @    6.4521,    0.9763,  339.9919
  5: monster     zombie_hell@arhara_dialog.script:spawn_trupak:1749                                                          (Адский зомби            )  @   12.8068,    0.1468,  376.9972
  6: monster     voen_zombied1@arhara_dialog.script:spawn_trupak:1750                                                        (pri_zombied_suicide     )  @   47.2120,    1.1623,  374.2412
  7: monster     zombie_plague_komar@arhara_dialog.script:spawn_trupak:1751                                                  (Комариный зомби         )  @  100.1323,    0.8667,  357.2286
  8: monster     girl_zombied2@arhara_dialog.script:spawn_trupak:1752                                                        (GENERATE_NAME_girls     )  @  113.2318,    0.8206,  320.9414
  9: monster     esc_arhara_stalker_zombied@arhara_dialog.script:spawn_trupak:1753                                           (GENERATE_NAME_stalker   )  @  -32.6533,    1.0320,  308.1441
 10: monster     esc_arhara_stalker_zombied1@arhara_dialog.script:spawn_trupak:1754                                          (GENERATE_NAME_stalker   )  @  -31.8088,    1.1013,  339.1908
 11: monster     zombie_plague@arhara_dialog.script:spawn_trupak:1755                                                        (Чумной зомби            )  @   55.6334,    1.0329,  392.3299
 12: monster     bloodsucker_strong@arhara_dialog.script:spawn_sos_sadik:5663                                                (Матёрый кровосос        )  @  136.7960,    3.9350,  -25.1530
 13: monster     snork_nosach@arhara_dialog.script:spawn_gig_sadik:5675                                                      (Упырь-носач             )  @   63.1610,   -2.3910,  -66.4760
 14: human       iskra_pri@buusty_dialog.script:spawn_parfumer_iskra_norman:5133                                             (Искра                   )  @  -99.5800,   -2.2100,  190.5400
 15: human       parfumer_pri@buusty_dialog.script:spawn_parfumer_iskra_norman:5134                                          (Парфюмер                )  @  -99.5800,   -2.2100,  190.5400
 16: human       kontrol_stalk_pri@buusty_dialog.script:spawn_parfumer_iskra_norman:5135                                     (Норман                  )  @  -99.5800,   -2.2100,  190.5400
 17: monster     m_controller_normal@kostya_dialog.script:961:968                                                            (Молодой контролёр       )  @   87.8540,   -2.5235,   86.0121
 18: monster     m_controller_normal_fat@kostya_dialog.script:961:969                                                        (Контролёр-толстяк       )  @   87.8540,   -2.5235,   86.0121
 19: monster     m_controller_old@kostya_dialog.script:961:970                                                               (Матёрый контролёр       )  @  160.5135,   -2.2141,   89.5465
 20: monster     m_controller_old_fat@kostya_dialog.script:961:971                                                           (Старый контролёр        )  @  160.5135,   -2.2141,   89.5465
 21: monster     pri_monolith_respawn_1@oksana_spawn.script:cinema_diary:553                                                 (sim_monolith_regular    )  @   -7.5450,    2.7780,  197.0150
 22: monster     pri_monolith_respawn_2@oksana_spawn.script:cinema_diary:554                                                 (sim_monolith_specnaz    )  @   -1.9650,    3.2280,  195.8650
 23: monster     pri_monolith_respawn_1@oksana_spawn.script:cinema_diary:555                                                 (sim_monolith_regular    )  @    2.3110,    3.6660,  197.9280
 24: monster     pri_monolith_respawn_3@oksana_spawn.script:cinema_diary:556                                                 (sim_monolith_master     )  @    2.7720,    3.6730,  205.4520
 25: monster     pri_monolith_respawn_3@oksana_spawn.script:cinema_diary:557                                                 (sim_monolith_master     )  @   -1.1030,    3.3160,  208.9980
 26: monster     pri_monolith_respawn_2@oksana_spawn.script:cinema_diary:558                                                 (sim_monolith_specnaz    )  @   -7.1260,    2.7790,  205.8240
 27: monster     tank_spawn_zasada@oksana_spawn.script:sports_sniper:571                                                     (GENERATE_NAME_stalker   )  @   65.3390,    8.4560,  277.1200
 28: monster     tank_spawn_zasada1@oksana_spawn.script:sports_sniper:572                                                    (GENERATE_NAME_stalker   )  @   -2.6690,    8.4560,  277.9760
 29: monster     tank_spawn_zasada2@oksana_spawn.script:sports_sniper:573                                                    (GENERATE_NAME_stalker   )  @   90.4510,    8.4570,  423.3940
 30: monster     tank_spawn_zasada3@oksana_spawn.script:sports_sniper:574                                                    (GENERATE_NAME_stalker   )  @   59.3220,    8.4580,  424.9920
 31: monster     tank_spawn_zasada4@oksana_spawn.script:sports_sniper:575                                                    (GENERATE_NAME_stalker   )  @   17.4200,    8.4560,  424.6430
 32: monster     tank_spawn_zasada5@oksana_spawn.script:sports_sniper:576                                                    (GENERATE_NAME_stalker   )  @  -21.6300,    8.4570,  424.3720
 33: monster     tank_spawn_zasada6@oksana_spawn.script:sports_sniper:577                                                    (GENERATE_NAME_stalker   )  @   42.5940,    8.4580,  277.1680
 34: monster     tank_spawn_zasada1@oksana_spawn.script:sports_sniper:578                                                    (GENERATE_NAME_stalker   )  @   33.1200,    8.4670,  277.5700
 35: monster     dcity_lastday_respawn_11@oksana_spawn.script:spawn_wooden_box:3038                                          (sim_lastday_master      )  @  106.1250,   -0.6840,  102.7570
 36: monster     dcity_lastday_respawn_22@oksana_spawn.script:spawn_wooden_box:3039                                          (sim_lastday_veteran     )  @  109.0090,   -0.6810,  103.3450
 37: monster     white_mackintosh@oksana_spawn.script:spawn_wooden_box:3040                                                  (GENERATE_NAME_stalker   )  @  110.8210,    0.4000,  117.6370
 38: monster     dcity_lastday_respawn_11@oksana_spawn.script:spawn_wooden_box:3041                                          (sim_lastday_master      )  @  105.0750,    0.4030,  153.3370
 39: monster     white_mackintosh@oksana_spawn.script:spawn_wooden_box:3042                                                  (GENERATE_NAME_stalker   )  @  104.8960,    3.9990,  111.6080
 40: monster     dcity_lastday_respawn_22@oksana_spawn.script:spawn_wooden_box:3043                                          (sim_lastday_veteran     )  @  105.3120,    3.9990,  157.5290
 41: monster     white_mackintosh@oksana_spawn.script:spawn_wooden_box:3044                                                  (GENERATE_NAME_stalker   )  @  111.8410,    3.9970,  121.2830
 42: monster     dcity_lastday_respawn_11@oksana_spawn.script:spawn_wooden_box:3045                                          (sim_lastday_master      )  @   99.4070,    3.9990,  129.8450
 43: monster     white_mackintosh@oksana_spawn.script:spawn_wooden_box:3046                                                  (GENERATE_NAME_stalker   )  @  101.5260,    3.9980,  123.8250
 44: human       ds_bandit_respawn_22@oksana_spawn.script:publicity_material_4:4736                                          (sim_bandit_general      )  @  193.4600,   -2.0030,  216.3960
 45: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:34                                                  (                        )  @  108.5500,   -2.4154,   59.7696
 46: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:35                                                  (                        )  @    8.9628,    9.3737,   88.4352
 47: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:36                                                  (                        )  @  -54.1304,    9.0447,   92.0132
 48: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:37                                                  (                        )  @  -78.4009,    9.0446,   92.1341
 49: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:38                                                  (                        )  @    2.1422,   -0.3776,  182.4788
 50: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:39                                                  (                        )  @  199.1886,   15.5258,   88.4992
 51: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:40                                                  (                        )  @  -19.7178,    3.9789,  195.5971
 52: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:41                                                  (                        )  @  -13.7984,    0.8449,  197.6367
 53: car/turret  auto_turret_2@radar_tasks.script:x10_turret_first_spawn:42                                                  (                        )  @    8.7186,    7.1374,   94.2766
 54: monster     snp_cp1@snp_spawn.script:n_inventory_box_cp1:249                                                            (GENERATE_NAME_lieutenant)  @  189.5482,   28.6632,  205.0263
 55: monster     snp_cp2@snp_spawn.script:n_inventory_box_cp1:250                                                            (GENERATE_NAME_lieutenant)  @   64.2812,    7.3218,  201.3911
 56: monster     snp_cp3@snp_spawn.script:n_inventory_box_cp1:251                                                            (GENERATE_NAME_lieutenant)  @  101.4530,   24.2524,  154.7143
 57: monster     snp_cp4@snp_spawn.script:n_inventory_box_cp1:252                                                            (GENERATE_NAME_lieutenant)  @  147.3357,   20.6500,  272.3061
 58: monster     bloodsucker_strong@spawn_zombi.script:spawn_krovosos9:23                                                    (Матёрый кровосос        )  @  202.8434,    2.8056,  212.3067
 59: monster     m_controller_old@spawn_zombi.script:spawn_kontroller2:26                                                    (Матёрый контролёр       )  @  161.8748,   -0.1268,  208.5453
 60: monster     bloodsucker_strong@spawn_zombi.script:spawn_krovosos10:29                                                   (Матёрый кровосос        )  @ -117.9132,    1.2032,  103.4955
 61: monster     bloodsucker_strong@spawn_zombi.script:spawn_krovosos11:32                                                   (Матёрый кровосос        )  @ -118.5970,   -2.0015,   98.6898
 62: monster     m_controller_old@spawn_zombi.script:spawn_kontroller3:35                                                    (Матёрый контролёр       )  @ -132.7112,   -2.3125,   75.2329
 63: car/turret  auto_turret_2@doctor_pomehi.script:doctor_pomehi_start:13                                                   (                        )  @   44.9665,   31.6425,   42.4957
 64: car/turret  auto_turret_2@doctor_pomehi.script:doctor_pomehi_start:14                                                   (                        )  @  -74.2528,   18.9031,   44.8998
 65: car/turret  auto_turret_2@doctor_pomehi.script:doctor_pomehi_start:15                                                   (                        )  @  102.4700,    8.4016,  -34.3163
 66: car/turret  auto_turret_2@doctor_pomehi.script:doctor_pomehi_start:16                                                   (                        )  @   26.0119,   19.7377,  155.0121
 67: car/turret  auto_turret_2@doctor_pomehi.script:doctor_pomehi_start:17                                                   (                        )  @   19.5704,   19.7367,  214.6770
 68: car/turret  auto_turret_2@doctor_pomehi.script:doctor_pomehi_start:18                                                   (                        )  @  -39.0257,   18.9054,  -18.7327
 69: monster     soldier_mines_regular@dragunov_aks.script:dragunov_aks_start:3                                              (GENERATE_NAME_captain   )  @  160.7022,    3.9987,  135.1786
 70: monster     soldier_mines_veteran@dragunov_aks.script:dragunov_aks_start:4                                              (GENERATE_NAME_sergeant  )  @  126.2285,    3.9991,  133.1949
 71: monster     soldier_mines_regular@dragunov_aks.script:dragunov_aks_start:5                                              (GENERATE_NAME_captain   )  @  192.9170,   -2.0014,  114.4697
 72: monster     soldier_mines_veteran@dragunov_aks.script:dragunov_aks_start:6                                              (GENERATE_NAME_sergeant  )  @  107.8205,   -0.6800,  102.4779
 73: monster     zasada_blockpost_1@dragunov_aks.script:dragunov_aks_start:7                                                 (GENERATE_NAME_lieutenant)  @  109.7412,   -2.3935,   64.8510
 74: monster     zasada_blockpost_2@dragunov_aks.script:dragunov_aks_start:8                                                 (GENERATE_NAME_lieutenant)  @   98.4146,   -1.7372,   45.8901
 75: monster     soldier_mines_veteran@dragunov_aks.script:dragunov_aks_start:9                                              (GENERATE_NAME_sergeant  )  @   38.0329,   -2.2209,   79.6333
 76: monster     soldier_mines_regular@dragunov_aks.script:dragunov_aks_start:10                                             (GENERATE_NAME_captain   )  @   28.9737,   -2.4038,   69.6087
 77: monster     agro_elite_soldier@dragunov_aks.script:dragunov_aks_start:11                                                (GENERATE_NAME_lieutenant)  @    1.8831,   -1.5421,   83.3943
 78: monster     zasada_blockpost_3@dragunov_aks.script:dragunov_aks_start:12                                                (GENERATE_NAME_lieutenant)  @   -7.1557,   -2.2214,   50.5527
 79: monster     agro_elite_soldier@dragunov_aks.script:dragunov_aks_start:13                                                (GENERATE_NAME_lieutenant)  @    0.3425,   -2.2220,  -10.6607
 80: monster     soldier_mines_regular@dragunov_aks.script:dragunov_aks_start:14                                             (GENERATE_NAME_captain   )  @   21.4198,   -2.2221,    1.1540
 81: monster     soldier_mines_veteran@dragunov_aks.script:dragunov_aks_start:15                                             (GENERATE_NAME_sergeant  )  @   62.4824,   -2.3923,   -5.7451
 82: monster     zasada_blockpost_1@dragunov_aks.script:dragunov_aks_start:16                                                (GENERATE_NAME_lieutenant)  @   65.3047,   -2.2414,  -16.9742
 83: monster     zasada_blockpost_2@dragunov_aks.script:dragunov_aks_start:17                                                (GENERATE_NAME_lieutenant)  @  103.4470,   -0.9274,  -27.9029
 84: monster     soldier_mines_regular@dragunov_aks.script:dragunov_aks_start:18                                             (GENERATE_NAME_captain   )  @  101.7384,   -2.3847,  -52.9225
 85: monster     soldier_mines_veteran@dragunov_aks.script:dragunov_aks_start:19                                             (GENERATE_NAME_sergeant  )  @   19.2382,   -2.2206,  -37.7422
 86: monster     agro_elite_soldier@dragunov_aks.script:dragunov_aks_start:21                                                (GENERATE_NAME_lieutenant)  @   56.7314,   -2.3583,  -93.1033
 87: monster     agro_elite_soldier@dragunov_aks.script:dragunov_aks_start:22                                                (GENERATE_NAME_lieutenant)  @   31.6950,   -2.3082,  -73.8863
 88: monster     agro_elite_rpg@dragunov_aks.script:dragunov_aks_start:23                                                    (GENERATE_NAME_lieutenant)  @  -47.4074,   -0.6749,  -17.8168
 89: monster     zasada_blockpost_1@dragunov_aks.script:dragunov_aks_start:24                                                (GENERATE_NAME_lieutenant)  @  -86.9299,    1.7216,   12.5371
 90: monster     soldier_mines_regular@dragunov_aks.script:dragunov_aks_start:25                                             (GENERATE_NAME_captain   )  @  -83.1206,   -2.4747,   31.6502
 91: monster     soldier_mines_veteran@dragunov_aks.script:dragunov_aks_start:26                                             (GENERATE_NAME_sergeant  )  @  -73.4230,   -2.1139,  -37.4599
 92: monster     zasada_blockpost_3@dragunov_aks.script:dragunov_aks_start:27                                                (GENERATE_NAME_lieutenant)  @  -53.4228,   -2.3907,  -61.5540
 93: monster     agro_elite_soldier@dragunov_aks.script:dragunov_aks_start:28                                                (GENERATE_NAME_lieutenant)  @  -28.1657,   -2.2553,  -79.3196
 94: monster     soldier_mines_veteran@dragunov_aks.script:dragunov_aks_start:29                                             (GENERATE_NAME_sergeant  )  @   34.6618,   -2.2221,  -52.7960
 95: monster     agro_elite_rpg@dragunov_aks.script:dragunov_aks_start:30                                                    (GENERATE_NAME_lieutenant)  @   -5.2332,   -2.2254,  -80.4606
 96: monster     agro_elite_soldier@dragunov_aks.script:dragunov_aks_start:31                                                (GENERATE_NAME_lieutenant)  @   68.6611,   -0.7890, -105.3968
 97: monster     major_akulov@dragunov_aks.script:dragunov_aks_start:32                                                      (Майор Акулов            )  @   -7.6847,   -2.2233, -114.5026
 98: car/turret  vehicle_btr@dragunov_aks.script:dragunov_aks_start:33                                                       (                        )  @  167.0007,   -2.2223,   91.8304
 99: car/turret  vehicle_btr@dragunov_aks.script:dragunov_aks_start:34                                                       (                        )  @   21.1624,   -0.6507,   95.5741
100: car/turret  vehicle_btr@dragunov_aks.script:dragunov_aks_start:35                                                       (                        )  @   76.6853,   -2.3910,  -64.7735
101: human       dragunof_npc@mines_dyak.script:adres_chertez_done:7                                                         (Драгунов                )  @  191.6754,    2.8056,  212.0197
102: monster     bw_killers2@new_radios.script:new_radios_adrenalin:28                                                       (Наёмник из Блэквотер    )  @ -106.0812,    1.7024,   12.7153
103: monster     bw_killers1@new_radios.script:new_radios_adrenalin:30                                                       (Наёмник из Блэквотер    )  @  -88.7512,    1.7210,   13.6309
104: monster     bw_killers2@new_radios.script:new_radios_adrenalin:31                                                       (Наёмник из Блэквотер    )  @ -101.5039,   -0.3116,  -16.6830
105: monster     bw_killers1@new_radios.script:new_radios_adrenalin:32                                                       (Наёмник из Блэквотер    )  @ -128.3956,    1.7020,   35.2670
106: monster     bw_killers2@new_radios.script:new_radios_adrenalin:33                                                       (Наёмник из Блэквотер    )  @ -104.0681,    1.6951,   35.4592
107: monster     bw_killers1@new_radios.script:new_radios_adrenalin:34                                                       (Наёмник из Блэквотер    )  @ -118.9071,    1.7028,   13.7663
108: car/turret  vehicle_apc@new_radios.script:new_radios_adrenalin:38                                                       (                        )  @ -141.8422,   -2.3920,   46.4249
109: monster     monolith_x8_regular@shurup_shron.script:shurup_shron_start:9                                                (sim_monolith_regular    )  @  103.8845,   -0.9362,  -46.5400
110: monster     monolith_x8_veteran@shurup_shron.script:shurup_shron_start:10                                               (sim_monolith_specnaz    )  @  102.9676,    3.9519,  -21.1971
111: monster     monolith_x8_regular@shurup_shron.script:shurup_shron_start:11                                               (sim_monolith_regular    )  @  103.3776,    3.9517,  -11.2476
112: monster     monolith_x8_master@shurup_shron.script:shurup_shron_start:12                                                (sim_monolith_master     )  @  103.8632,   -0.9604,  -15.4540
113: monster     tank_spawn_zasada7@shurup_shron.script:shurup_shron_start:13                                                (GENERATE_NAME_stalker   )  @  126.9750,   -0.9454,  -22.4203
114: monster     monolith_x8_regular@shurup_shron.script:shurup_shron_start:14                                               (sim_monolith_regular    )  @  133.2007,   -0.9337,  -53.5072
115: monster     monolith_x8_veteran@shurup_shron.script:shurup_shron_start:15                                               (sim_monolith_specnaz    )  @  132.7861,    3.9371,  -26.2603
116: monster     tank_x003@shurup_shron.script:shurup_shron_start:16                                                         (GENERATE_NAME_stalker   )  @  132.8367,   -0.9292,  -31.7830
117: monster     monolith_x8_regular@shurup_shron.script:shurup_shron_start:17                                               (sim_monolith_regular    )  @   70.5483,   -2.2319,  -14.1667
118: monster     monolith_x8_veteran@shurup_shron.script:shurup_shron_start:18                                               (sim_monolith_specnaz    )  @   81.0529,   -7.3956,   26.2376
119: monster     monolith_x8_regular@shurup_shron.script:shurup_shron_start:19                                               (sim_monolith_regular    )  @   79.2355,   -7.3914,   12.8779
120: monster     monolith_x8_master@shurup_shron.script:shurup_shron_start:20                                                (sim_monolith_master     )  @   99.5031,   -7.3919,   18.1054
121: monster     tank_spawn_zasada7@shurup_shron.script:shurup_shron_start:21                                                (GENERATE_NAME_stalker   )  @  100.1085,   -7.3910,   12.2904
122: monster     monolith_x8_regular@shurup_shron.script:shurup_shron_start:23                                               (sim_monolith_regular    )  @  114.0770,   -4.1001,   52.8577
123: monster     monolith_x8_veteran@shurup_shron.script:shurup_shron_start:24                                               (sim_monolith_specnaz    )  @   99.3458,   -2.2209,   61.3506
124: car/turret  auto_turret_2@shurup_shron.script:shurup_shron_start:25                                                     (                        )  @   99.4567,    3.3472,  -25.7372
125: car/turret  auto_turret_2@shurup_shron.script:shurup_shron_start:26                                                     (                        )  @   99.4243,   -0.7545,  -66.4680
126: human       klesh_npc@svoboda_peremirie.script:klesh_npc_spawn:69                                                       (Клещ                    )  @  -81.7093,    0.0102,  269.9920
127: monster     ohota_lican@svoboda_peremirie.script:ohota_himera_start:76                                                  (Ликан                   )  @   29.7738,    2.0114,  352.2991
128: human       golem_npc@taynik_golem.script:golem_npc_spawn:3                                                             (Голем                   )  @ -121.4710,   -3.4733,  103.7636
129: monster     kontrik_labx16@taynik_golem.script:golem_npc_spawn:5                                                        (Матёрый контролёр       )  @ -112.6062,   -2.0006,  102.1545
130: monster     zombie_hell@taynik_golem.script:golem_npc_spawn:6                                                           (Адский зомби            )  @  -97.6257,   -2.2072,  114.5196
131: monster     zombie_immortal@taynik_golem.script:golem_npc_spawn:7                                                       (Ведьма                  )  @  -84.3827,   -0.0069,   96.4465
132: monster     zombie_hell1@taynik_golem.script:golem_npc_spawn:8                                                          (Зомби-странник          )  @ -100.9035,   -2.2364,   61.2484
133: monster     zombie_ghost@taynik_golem.script:golem_npc_spawn:9                                                          (Зомби-призрак           )  @  -80.0832,   -2.3908,   44.5005
134: monster     voen_zombied1@taynik_golem.script:golem_npc_spawn:10                                                        (pri_zombied_suicide     )  @  -80.0832,   -2.3908,   44.5005
135: monster     zombie_hell14@taynik_golem.script:golem_npc_spawn:11                                                        (Зомби в мешковине       )  @  -58.6639,   -1.7323,   48.1752
136: monster     zombie_trup2@taynik_golem.script:golem_npc_spawn:12                                                         (Полуразложившийся зомби )  @  -50.7746,   -0.0026,   95.9533
137: monster     esc_arhara_stalker_zombied@taynik_golem.script:golem_npc_spawn:13                                           (GENERATE_NAME_stalker   )  @  -31.2471,   -2.3948,   72.1848
138: monster     voen_zombied2@taynik_golem.script:golem_npc_spawn:14                                                        (pri_zombied_veteran     )  @    0.6462,   -0.6491,  100.6043
139: monster     zombie_plague_komar@taynik_golem.script:golem_npc_spawn:15                                                  (Комариный зомби         )  @    0.6462,   -0.6491,  100.6043
140: monster     esc_arhara_stalker_zombied1@taynik_golem.script:golem_npc_spawn:16                                          (GENERATE_NAME_stalker   )  @   28.6999,   -2.2209,   80.7610
141: monster     zombie_beee@taynik_golem.script:golem_npc_spawn:17                                                          (Взрывающийся зомби      )  @  -21.3501,   -1.7337,   42.7514
142: monster     monolith_x8_master@taynik_virus.script:unknown_gp_start:4                                                   (sim_monolith_master     )  @   -5.4549,   -5.1615,  199.6213
143: monster     tank_spawn_zasada3@taynik_virus.script:unknown_gp_start:5                                                   (GENERATE_NAME_stalker   )  @  -20.2541,    0.0947,  205.7955
144: monster     monolith_x8_master@taynik_virus.script:unknown_gp_start:6                                                   (sim_monolith_master     )  @  -27.1101,    4.6920,  182.5356
145: monster     monolith_x8_veteran@taynik_virus.script:unknown_gp_start:7                                                  (sim_monolith_specnaz    )  @    5.1370,    3.9415,  205.8507
146: monster     monolith_x8_veteran@taynik_virus.script:unknown_gp_start:8                                                  (sim_monolith_specnaz    )  @   -9.2928,    2.6016,  205.8774
147: monster     tank_spawn_zasada7@taynik_virus.script:unknown_gp_start:9                                                   (GENERATE_NAME_stalker   )  @   15.1804,    5.0890,  185.2383
148: monster     monolith_x8_veteran@taynik_virus.script:unknown_gp_start:10                                                 (sim_monolith_specnaz    )  @   36.6230,    5.1029,  176.1993
149: monster     monolith_x8_regular@taynik_virus.script:unknown_gp_start:11                                                 (sim_monolith_regular    )  @   24.6895,    5.4187,  152.3995
150: monster     tank_x003@taynik_virus.script:unknown_gp_start:12                                                           (GENERATE_NAME_stalker   )  @  -20.5899,    5.4224,  152.9046
151: monster     monolith_x8_regular@taynik_virus.script:unknown_gp_start:13                                                 (sim_monolith_regular    )  @  -15.5694,   -0.0057,  159.3553
152: monster     monolith_x8_master@taynik_virus.script:unknown_gp_start:14                                                  (sim_monolith_master     )  @   20.5335,   -0.0016,  165.7624
153: monster     monolith_x8_regular@taynik_virus.script:unknown_gp_start:15                                                 (sim_monolith_regular    )  @   30.5317,   -0.0001,  182.8246
154: monster     monolith_x8_veteran@taynik_virus.script:unknown_gp_start:16                                                 (sim_monolith_specnaz    )  @  -38.1162,    1.1360,  173.8333
155: monster     tank_spawn_zasada7@taynik_virus.script:unknown_gp_start:17                                                  (GENERATE_NAME_stalker   )  @  -15.4677,   -0.0020,  214.0356
156: monster     monolith_x8_veteran@taynik_virus.script:unknown_gp_start:18                                                 (sim_monolith_specnaz    )  @   26.0063,    5.4206,  213.8336
157: monster     monolith_x8_veteran@taynik_virus.script:unknown_gp_start:19                                                 (sim_monolith_specnaz    )  @   60.5196,    5.4224,  217.1769
158: monster     monolith_x8_regular@taynik_virus.script:unknown_gp_start:20                                                 (sim_monolith_regular    )  @   46.3937,   -0.0005,  214.9879
159: monster     monolith_x8_regular@taynik_virus.script:unknown_gp_start:21                                                 (sim_monolith_regular    )  @  -32.4192,    3.6537,  138.6236
160: monster     bw_killers1@taynik_virus.script:unknown_gp_start:22                                                         (Наёмник из Блэквотер    )  @  -45.6491,   -0.0016,  145.5075
161: monster     bw_killers2@taynik_virus.script:unknown_gp_start:23                                                         (Наёмник из Блэквотер    )  @  -37.9576,   -0.0013,  130.5583
162: monster     bw_killers1@taynik_virus.script:unknown_gp_start:24                                                         (Наёмник из Блэквотер    )  @  -55.6359,   -0.3417,   94.7686
163: monster     bw_killers2@taynik_virus.script:unknown_gp_start:25                                                         (Наёмник из Блэквотер    )  @  -52.1980,   -1.7323,   50.1708
164: monster     bw_killers1@taynik_virus.script:unknown_gp_start:26                                                         (Наёмник из Блэквотер    )  @  -15.4232,   -0.6479,   96.9589
165: monster     bw_killers2@taynik_virus.script:unknown_gp_start:27                                                         (Наёмник из Блэквотер    )  @   35.6430,   -0.3115,  105.3419
166: monster     bw_killers1@taynik_virus.script:unknown_gp_start:28                                                         (Наёмник из Блэквотер    )  @   62.3976,   -0.0002,  120.8968
167: monster     bw_killers2@taynik_virus.script:unknown_gp_start:29                                                         (Наёмник из Блэквотер    )  @   54.2667,   -0.0003,  143.1838
168: monster     bw_killers1@taynik_virus.script:unknown_gp_start:30                                                         (Наёмник из Блэквотер    )  @   87.8970,   -0.6804,  155.5756
169: monster     bw_killers2@taynik_virus.script:unknown_gp_start:31                                                         (Наёмник из Блэквотер    )  @   35.0971,   -2.3999,   71.9008
170: monster     monolith_x8_master@taynik_virus.script:taynik_virus_start:41                                                (sim_monolith_master     )  @  104.7950,   -0.9365,  -40.7583
171: monster     tank_spawn_zasada4@taynik_virus.script:taynik_virus_start:42                                                (GENERATE_NAME_stalker   )  @  104.5080,   -0.9436,  -20.1834
172: monster     monolith_x8_veteran@taynik_virus.script:taynik_virus_start:43                                               (sim_monolith_specnaz    )  @  103.3194,    3.9511,  -21.2784
173: monster     monolith_x8_veteran@taynik_virus.script:taynik_virus_start:44                                               (sim_monolith_specnaz    )  @  132.8037,   -0.9358,  -53.4716
174: monster     monolith_x8_regular@taynik_virus.script:taynik_virus_start:45                                               (sim_monolith_regular    )  @   78.5213,   -2.3921,   -6.6204
175: car/turret  auto_turret_2@taynik_virus.script:taynik_virus_start:46                                                     (                        )  @   51.4005,   18.9058,  -30.6603
176: car/turret  auto_turret_2@taynik_virus.script:taynik_virus_start:47                                                     (                        )  @   23.4145,   18.9058,  -30.5004
177: car/turret  auto_turret_2@turret_kep.script:turret_kep_start:4                                                          (                        )  @  -16.0336,   -3.2016,  196.4020
