  0: human       business_man@oksana_spawn.script:corpse_shuttle:2561                                                (Гоша Челнок             )  @  -44.9270,  -16.4630,  -42.3770
  1: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2567                                    (GENERATE_NAME_bandit    )  @  -76.0300,  -16.4340,  -47.7940
  2: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2568                                    (GENERATE_NAME_bandit    )  @  -84.6730,  -16.4630,  -55.3520
  3: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2569                                    (GENERATE_NAME_bandit    )  @  -47.6930,  -16.4630,  -38.0500
  4: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2570                                    (GENERATE_NAME_bandit    )  @  -40.6240,   -3.8890,   24.5600
  5: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2571                                    (GENERATE_NAME_bandit    )  @  -94.2830,  -16.4650,   -4.8170
  6: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2572                                    (GENERATE_NAME_bandit    )  @  -79.7400,  -16.3350,   10.9010
  7: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2573                                    (GENERATE_NAME_bandit    )  @  -81.6000,  -16.4650,    5.7700
  8: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2574                                    (GENERATE_NAME_bandit    )  @  -58.8120,   -6.4980,    9.2060
  9: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2575                                    (GENERATE_NAME_bandit    )  @  -59.4400,   -6.5390,   18.6940
 10: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2576                                    (GENERATE_NAME_bandit    )  @ -104.8320,  -16.4620,  -35.3860
 11: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2577                                    (GENERATE_NAME_bandit    )  @  -98.9060,  -16.4720,  -15.9830
 12: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2578                                    (GENERATE_NAME_bandit    )  @  -67.8460,  -16.4640,  -42.3890
 13: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2579                                    (GENERATE_NAME_bandit    )  @  -77.0670,  -16.5100, -112.2900
 14: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2580                                    (GENERATE_NAME_bandit    )  @  -79.1140,  -16.4010, -106.4190
 15: monster     white_mackintosh_1@oksana_spawn.script:raincoat_underground:2581                                    (GENERATE_NAME_bandit    )  @  -73.6790,  -16.5110, -116.6990
 16: monster     bloodsucker_strong@oksana_spawn.script:subway_monsters_1:2586                                       (Матёрый кровосос        )  @  -79.7810,  -16.4450,   18.1480
 17: monster     snork_normal@oksana_spawn.script:subway_monsters_1:2587                                             (Взрослый снорк          )  @  -81.6000,  -16.4650,    5.7700
 18: monster     snork_normal@oksana_spawn.script:subway_monsters_2:2592                                             (Взрослый снорк          )  @  -76.0300,  -16.4340,  -47.7940
 19: monster     snork_normal@oksana_spawn.script:subway_monsters_2:2593                                             (Взрослый снорк          )  @  -67.8460,  -16.4640,  -42.3890
 20: monster     bloodsucker_strong@oksana_spawn.script:subway_monsters_2:2594                                       (Матёрый кровосос        )  @  -84.6730,  -16.4630,  -55.3520
 21: monster     snork_normal@oksana_spawn.script:subway_monsters_3:2599                                             (Взрослый снорк          )  @  -58.8120,   -6.4980,    9.2060
 22: monster     snork_normal@oksana_spawn.script:subway_monsters_3:2600                                             (Взрослый снорк          )  @  -50.3680,   -6.5190,   11.9720
 23: monster     snork_normal@oksana_spawn.script:subway_monsters_3:2601                                             (Взрослый снорк          )  @  -51.3630,   -6.5260,   19.7930
 24: monster     bloodsucker_strong@oksana_spawn.script:subway_monsters_3:2602                                       (Матёрый кровосос        )  @  -59.4400,   -6.5390,   18.6940
 25: human       ds_bandit_respawn_199@oksana_spawn.script:family_relics_3:5027                                      (sim_soldier_specnaz     )  @  -74.0020,   -4.0150, -135.5750
 26: monster     bloodsucker_normal@xr_effects.script:spawn_krovosos:2088                                            (Тёмный кровосос         )  @ -113.2749,  -16.4630,  -53.7494
 27: monster     bloodsucker_normal@xr_effects.script:spawn_krovosos2:2092                                           (Тёмный кровосос         )  @ -105.3224,  -16.4630,  -18.5576
 28: monster     soldier_mines_veteran@akim_journal.script:agro_journal_spawn:22                                     (GENERATE_NAME_sergeant  )  @  -72.0642,   -6.4984,   10.6438
 29: monster     soldier_mines_regular@akim_journal.script:agro_journal_spawn:23                                     (GENERATE_NAME_captain   )  @  -71.7855,   -6.4981,   16.5694
 30: monster     soldier_mines_veteran@akim_journal.script:agro_journal_spawn:24                                     (GENERATE_NAME_sergeant  )  @  -69.4367,   -6.5001,   14.1887
 31: monster     soldier_mines_veteran@akim_journal.script:agro_journal_spawn:25                                     (GENERATE_NAME_sergeant  )  @  -69.1186,   -6.5021,   11.0279
 32: monster     spider@dolgovazyi_toolkit.script:plichko_toolkit:30                                                 (Паук                    )  @  -94.6674,   -4.3921, -143.4548
 33: monster     spider@dolgovazyi_toolkit.script:plichko_toolkit:31                                                 (Паук                    )  @ -100.1869,  -16.4494,  -97.7560
 34: monster     spider@dolgovazyi_toolkit.script:plichko_toolkit:32                                                 (Паук                    )  @  -88.5974,  -16.4678,  -79.6274
 35: monster     arahnid@dolgovazyi_toolkit.script:plichko_toolkit:33                                                (Арахнид                 )  @  -88.5974,  -16.4678,  -79.6274
 36: monster     arahnid@dolgovazyi_toolkit.script:plichko_toolkit:34                                                (Арахнид                 )  @  -60.0269,  -16.4627,  -37.8013
 37: monster     arahnid_strong@dolgovazyi_toolkit.script:plichko_toolkit:35                                         (Жёлтый арахнид          )  @ -107.9533,  -16.4618,  -33.0242
 38: monster     arahnid_strong@dolgovazyi_toolkit.script:plichko_toolkit:36                                         (Жёлтый арахнид          )  @  -70.5376,   -4.0942, -125.0044
 39: human       journalist_npc@foto_ohota.script:foto_ohota_spawn:4                                                 (Сеня Журналист          )  @  -65.4483,   -4.4372, -143.6870
 40: monster     alfa_omon@mortal_kombat.script:mortal_kombat_spawn:5                                                (GENERATE_NAME_lieutenant)  @  -63.9589,   -6.4901,   10.8472
 41: monster     alfa_omon2@mortal_kombat.script:mortal_kombat_spawn:6                                               (GENERATE_NAME_lieutenant)  @  -44.5997,  -14.6473,  -41.0189
 42: monster     alfa_omon3@mortal_kombat.script:mortal_kombat_spawn:7                                               (GENERATE_NAME_lieutenant)  @  -99.9994,  -16.4504, -101.6264
 43: monster     alfa_omon4@mortal_kombat.script:mortal_kombat_spawn:8                                               (GENERATE_NAME_lieutenant)  @  -68.4202,   -4.5040, -142.8015
 44: monster     soldier_mines_regular@yakut_treasure.script:yakut_treasure_start:12                                 (GENERATE_NAME_captain   )  @  -80.5460,  -16.4610,  -52.6760
 45: monster     soldier_mines_veteran@yakut_treasure.script:yakut_treasure_start:13                                 (GENERATE_NAME_sergeant  )  @  -88.6520,  -16.4620,  -80.1090
 46: monster     soldier_mines_regular@yakut_treasure.script:yakut_treasure_start:14                                 (GENERATE_NAME_captain   )  @ -106.1500,  -16.4660,  -76.4990
 47: monster     soldier_mines_veteran@yakut_treasure.script:yakut_treasure_start:15                                 (GENERATE_NAME_sergeant  )  @ -120.6760,  -14.9910,  -40.3970
 48: monster     soldier_mines_regular@yakut_treasure.script:yakut_treasure_start:16                                 (GENERATE_NAME_captain   )  @ -110.6390,  -16.4670,  -36.5590
 49: monster     soldier_mines_veteran@yakut_treasure.script:yakut_treasure_start:17                                 (GENERATE_NAME_sergeant  )  @ -107.4270,  -14.2340,  -26.4640
 50: monster     soldier_mines_regular@yakut_treasure.script:yakut_treasure_start:18                                 (GENERATE_NAME_captain   )  @  -97.2840,  -16.4620,  -16.6590
 51: monster     soldier_mines_veteran@yakut_treasure.script:yakut_treasure_start:19                                 (GENERATE_NAME_sergeant  )  @  -87.2300,  -16.4670,  -65.9020
