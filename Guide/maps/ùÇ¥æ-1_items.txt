  0: LevelWarper level_changer                                                                                                                   @  117.7190,   -0.1010,  -76.4010 1 x 1
  1: item        Criminal note 21                                                       note_zone_21             ("ТЧ - Зов Монолита"          ) @  929.2860,   -0.0014, -200.5005
  2: manual item sumka_arhara@arhara_dialog.script:sahar_sumka_start:174                sumka_arhara             (Сумка Сахарова               ) @  274.5400,    0.9115, -216.4744
  3: manual item publicity_material_2@oksana_spawn.script:family_relics_5:5061          publicity_material_2     (Небольшой рюкзак             ) @  918.9580,   -0.0990, -394.1910
  4: manual item foto_father@oksana_spawn.script:family_relics_5:5063                   foto_father              (Последняя Фотография         ) @  860.1370,    0.7860,  -53.7390
  5: manual item voen3_toolkit r0@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  918.9290,    0.4900, -163.3900
  6: manual item voen3_toolkit r1@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  921.3170,    0.4950, -194.1350
  7: manual item voen3_toolkit r2@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  919.7350,    0.4770, -212.2090
  8: manual item voen3_toolkit r3@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  440.7310,    0.5030,  -45.2730
  9: manual item voen3_toolkit r4@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  431.3920,    0.7620,  -63.2210
 10: manual item voen3_toolkit r5@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  371.5910,    0.6030,  -40.3610
 11: manual item voen3_toolkit r6@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  407.4110,    0.5030,  -95.6920
 12: manual item voen3_toolkit r7@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  359.8450,    0.6030, -139.3530
 13: manual item voen3_toolkit r8@dolg_btr.script:dolg_btr_done:30                      voen3_toolkit            (Армейские инструменты ОРК-27м) @  288.0990,    0.5030,  -76.7260
 14: manual item kuznec_koloda@kuznecov_ugroza2.script:kuznecov_ugroza2_start:25        kuznec_koloda            (Игральная карта              ) @  337.0663,    3.6115, -160.3679
 15: item        aes_ammo_og-7b                                                         ammo_og-7b               (ПГ-7ВЛ «Луч»                 ) @  283.5769,    0.4366, -222.5130
 16: item        aes_ammo_og-7b_0000                                                    ammo_og-7b               (ПГ-7ВЛ «Луч»                 ) @  283.8147,    0.4366, -222.6862
 17: item        aes_grenade_f_0000                                                     grenade_f1               (Граната Ф-1                  ) @  271.8820,    0.4366, -220.8509
 18: item        aes_grenade_f_0001                                                     grenade_f1               (Граната Ф-1                  ) @  272.0033,    0.4366, -220.9138
 19: item        aes_grenade_f_0002                                                     grenade_f1               (Граната Ф-1                  ) @  271.7600,    0.4366, -220.7867
 20: item        aes_grenade_f_0003                                                     grenade_f1               (Граната Ф-1                  ) @  271.6795,    0.4366, -220.6637
 21: LevelWarper chaes_to_generators_shape_0                                                                                                     @  369.9242,    0.1498, -247.8283 1.36159992218018 x 1.36159992218018
 22: LevelWarper exit_to_pripyat_from_st1_shape_0                                                                                                @  917.9296,   -0.0999, -404.1170 11.2959985733032 x 3.42279982566833
 23: LevelWarper exit_to_sarcofag_01_shape_0                                                                                                     @  367.7400,   -8.0400,   34.9100 3.36159992218018 x 3.36159992218018
 24: item        aes_wpn_binoc                                                          wpn_binoc_m1             (Бинокль БПОс                 ) @  273.5397,    0.8638, -216.4397
 25: item        aes_wpn_rpg7                                                           wpn_rpg7                 (РПГ-7                        ) @  283.5230,    1.1111, -223.6177
