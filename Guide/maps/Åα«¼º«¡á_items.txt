  0: item        balon_0001                                      amk_ballon               (Баллон с коллоидным газом) @  -53.2250,  -13.3069,  680.8068
  1: item        dump_antirad                                    antirad                  (Antirad                  ) @   19.7004,    8.1593,  264.3830
  2: item        dump_antirad_0000                               antirad                  (Antirad                  ) @ -120.9877,  -14.3722,  839.3610
  3: item        dump_conserva_0000                              conserva                 (Conserva                 ) @  -56.3257,   -9.5152,  660.4605
  4: item        dump_conserva_0001                              conserva                 (Conserva                 ) @  -56.8088,   -9.7969,  660.8797
  5: item        dump_conserva_0002                              conserva                 (Conserva                 ) @  -53.2891,   -9.7969,  660.7946
  6: item        dump_conserva_0003                              conserva                 (Conserva                 ) @  -52.4339,   -9.5152,  656.5175
  7: item        dump_medkit                                     medkit                   (Medkit                   ) @   61.3067,   -9.2993,  898.3188
  8: item        dump_medkit_0000                                medkit                   (Medkit                   ) @    3.4799,    6.0031,  141.2820
  9: item        dump_medkit_0001                                medkit                   (Medkit                   ) @  -14.0980,    5.1031,   14.6517
 10: item        dump_medkit_army                                medkit_army              (Medkit_army              ) @   19.1633,   -6.4192,   20.0437
 11: item        dump_vodka                                      vodka                    (Vodka                    ) @  -69.9775,  -12.2152,  667.5003
 12: item        dump_vodka_0000                                 vodka                    (Vodka                    ) @   21.6948,   -6.3474,   17.6528
 13: item        dump_vodka_0001                                 vodka                    (Vodka                    ) @   18.0098,   -6.0640,   19.5506
 14: item        dump_vodka_0002                                 vodka                    (Vodka                    ) @  -33.6510,   -7.8469,  671.0427
