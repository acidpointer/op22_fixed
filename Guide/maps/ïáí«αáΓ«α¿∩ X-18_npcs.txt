  0: monster     naemnik_regular@akill.script:system_blok_spawn:132                                                       (sim_killer_general       )  @  -0.8685,   9.3198,  -7.7809
  1: monster     naemnik_veteran@akill.script:system_blok_spawn:133                                                       (sim_killer_veteran       )  @  -7.8673,   9.1754,   1.2526
  2: monster     naemnik_regular@akill.script:system_blok_spawn:134                                                       (sim_killer_general       )  @   0.4633,   4.1887,   3.1147
  3: monster     naemnik_veteran@akill.script:system_blok_spawn:135                                                       (sim_killer_veteran       )  @  18.4353,   4.1890, -12.8737
  4: monster     naemnik_regular@akill.script:system_blok_spawn:136                                                       (sim_killer_general       )  @   0.4720, -10.8193,  -5.3833
  5: monster     pri_arhara_naemnik1@akill.script:system_blok_spawn:137                                                   (sim_killer_master        )  @ -23.3026, -10.8171,  -4.1788
  6: monster     pri_arhara_naemnik1@akill.script:system_blok_spawn:139                                                   (sim_killer_master        )  @  -2.4418, -10.8145,  18.5123
  7: monster     burer_electro@akill.script:fanat_teleport_spawn:168                                                      (Электробюрер             )  @   0.5572, -10.8171,  -7.2836
  8: monster     m_poltergeist_dd@akill.script:fanat_teleport_spawn:169                                                   (Ползучий пиро-полтергейст)  @  -1.9403, -10.8144,  20.6504
  9: monster     polter_xray@akill.script:fanat_teleport_spawn:170                                                        (Призрачный полтергейст   )  @ -27.8158, -10.8174,  -7.0143
 10: monster     electro_polter@akill.script:fanat_teleport_spawn:171                                                     (Электрополтергейст       )  @   1.3620,   4.1898,  -0.8215
 11: monster     psyonik@akill.script:fanat_teleport_spawn:172                                                            (Псионик                  )  @   1.3620,   4.1898,  -0.8215
 12: monster     ystinov_npc@buusty_dialog.script:izdelie_foton_docent:3217                                               (Профессор Устинов        )  @  28.1200,  -6.6200, -15.3100
 13: monster     m_poltergeist_tele_outdoor@buusty_dialog.script:karlik_ystinov_start:3263                                (Матёрый пси-полтергейст  )  @   9.7530, -12.4300,  22.8080
 14: monster     m_poltergeist_tele_outdoor@buusty_dialog.script:karlik_ystinov_start:3264                                (Матёрый пси-полтергейст  )  @   8.8370, -10.8140,  -4.4450
 15: monster     m_poltergeist_tele_outdoor@buusty_dialog.script:karlik_ystinov_start:3265                                (Матёрый пси-полтергейст  )  @ -31.5530, -10.8210,   4.3820
 16: monster     m_poltergeist_tele_outdoor@buusty_dialog.script:karlik_ystinov_start:3266                                (Матёрый пси-полтергейст  )  @ -22.6300, -10.8110, -20.0000
 17: monster     m_poltergeist_tele_outdoor@buusty_dialog.script:karlik_ystinov_start:3267                                (Матёрый пси-полтергейст  )  @  -6.2380,  -8.3160, -14.1850
 18: monster     m_poltergeist_normal_flame@buusty_dialog.script:karlik_ystinov_start:3268                                (Пиро-полтергейст         )  @ -31.5530, -10.8210,   4.3820
 19: monster     m_poltergeist_normal_flame@buusty_dialog.script:karlik_ystinov_start:3269                                (Пиро-полтергейст         )  @ -22.6300, -10.8110, -20.0000
 20: monster     m_poltergeist_normal_flame@buusty_dialog.script:karlik_ystinov_start:3270                                (Пиро-полтергейст         )  @  -6.2380,  -8.3160, -14.1850
 21: human       dar_corpse_kalugin1@oksana_spawn.script:kalugin_corpse:936                                               (Профессор Калугин        )  @  23.7370,   4.1890,  17.2570
 22: monster     tank_spawn_zasada@oksana_spawn.script:bioambush_1:962                                                    (GENERATE_NAME_stalker    )  @  30.4340,   4.1890,  11.4500
 23: monster     tank_spawn_zasada1@oksana_spawn.script:bioambush_1:963                                                   (GENERATE_NAME_stalker    )  @ -15.8160,   4.1880,   8.2470
 24: monster     tank_spawn_zasada2@oksana_spawn.script:bioambush_1:964                                                   (GENERATE_NAME_stalker    )  @  -7.9520,   4.2060,  -0.7530
 25: monster     tank_spawn_zasada3@oksana_spawn.script:bioambush_1:965                                                   (GENERATE_NAME_stalker    )  @  18.8330,   4.1890, -13.0960
 26: monster     tank_spawn_zasada4@oksana_spawn.script:bioambush_1:966                                                   (GENERATE_NAME_stalker    )  @  -8.1030,   9.1740,   0.2920
 27: monster     tank_spawn_zasada5@oksana_spawn.script:bioambush_1:967                                                   (GENERATE_NAME_stalker    )  @  -5.1610,   4.1900,   7.4800
 28: monster     tank_spawn_zasada6@oksana_spawn.script:bioambush_1:968                                                   (GENERATE_NAME_stalker    )  @   6.2820,   4.1870,  -0.0106
 29: monster     snork_normal@oksana_spawn.script:monsters_attack:982                                                     (Взрослый снорк           )  @  22.1660, -14.8170,  77.3710
 30: monster     snork_normal@oksana_spawn.script:monsters_attack:983                                                     (Взрослый снорк           )  @  29.9000,  -6.6200, -13.3850
 31: monster     burer_strong@oksana_spawn.script:monsters_attack:984                                                     (Матёрый бюрер            )  @  38.8060,  -8.6500, -13.0070
 32: monster     snork_normal@oksana_spawn.script:monsters_attack:985                                                     (Взрослый снорк           )  @  34.0230, -10.8150,   4.2390
 33: monster     snork_stronger@oksana_spawn.script:monsters_attack:986                                                   (Болотный снорк           )  @ -32.4850, -10.8130, -20.4220
 34: monster     snork_normal@oksana_spawn.script:monsters_attack:987                                                     (Взрослый снорк           )  @ -41.1740, -10.8150,   4.7560
 35: monster     burer_strong@oksana_spawn.script:monsters_attack:988                                                     (Матёрый бюрер            )  @  -2.8770, -10.8150,  27.3850
 36: monster     snork_jumper@oksana_spawn.script:monsters_attack:989                                                     (Крылатый снорк           )  @  16.1930, -12.4290,  17.3550
 37: monster     snork_normal@oksana_spawn.script:monsters_attack:990                                                     (Взрослый снорк           )  @ -24.6760, -10.7830,  -4.1390
 38: monster     m_controller_act@oksana_spawn.script:monsters_attack:991                                                 (Быстрый контролёр        )  @ -43.9130, -13.0060, -20.3730
 39: monster     snork_normal@oksana_spawn.script:monsters_attack:992                                                     (Взрослый снорк           )  @ -25.3420, -10.8140,  -4.0760
 40: monster     snork_stronger@oksana_spawn.script:monsters_attack:993                                                   (Болотный снорк           )  @   6.2760, -12.4310,   8.2870
 41: monster     snork_normal@oksana_spawn.script:monsters_attack:994                                                     (Взрослый снорк           )  @  10.0330,   4.1890,   6.4200
 42: monster     snork_normal@oksana_spawn.script:monsters_attack:995                                                     (Взрослый снорк           )  @  24.4270,   4.1880,  16.2120
 43: monster     m_poltergeist_dd@oksana_spawn.script:monsters_attack:996                                                 (Ползучий пиро-полтергейст)  @  -5.1610,   4.1900,   7.4800
 44: monster     snork_normal@oksana_spawn.script:monsters_attack:997                                                     (Взрослый снорк           )  @ -10.1370,   4.1890,  -5.0150
 45: monster     snork_normal@oksana_spawn.script:monsters_attack:998                                                     (Взрослый снорк           )  @   3.5130,   4.1980, -17.0120
 46: monster     snork_jumper@oksana_spawn.script:monsters_attack:999                                                     (Крылатый снорк           )  @  18.8850,   4.1880, -13.2730
 47: monster     snork_normal@oksana_spawn.script:monsters_attack:1000                                                    (Взрослый снорк           )  @  10.6210, -16.3470,  55.0150
 48: monster     snork_normal@oksana_spawn.script:monsters_attack:1001                                                    (Взрослый снорк           )  @  29.0120, -13.5340,  39.8530
 49: monster     snork_stronger@oksana_spawn.script:monsters_attack:1002                                                  (Болотный снорк           )  @  14.2500, -16.0940,  32.3040
 50: monster     snork_normal@oksana_spawn.script:monsters_attack:1003                                                    (Взрослый снорк           )  @   8.9650, -16.0990,  32.2890
 51: monster     m_controller_act@oksana_spawn.script:monsters_attack:1004                                                (Быстрый контролёр        )  @  -3.2480, -16.3440,  41.6250
 52: monster     snork_normal@oksana_spawn.script:monsters_attack:1005                                                    (Взрослый снорк           )  @  28.2050, -10.8140,   4.5620
 53: monster     scelet_strong@oksana_spawn.script:monsters_attack:1006                                                   (Зомби-скелет             )  @  40.6710, -10.8150,  -7.0520
 54: monster     scelet_strong@oksana_spawn.script:monsters_attack:1007                                                   (Зомби-скелет             )  @  40.4370,  -6.2850, -17.8600
 55: monster     scelet_strong@oksana_spawn.script:monsters_attack:1008                                                   (Зомби-скелет             )  @  18.6490,  -6.6200, -18.3810
 56: monster     snork_normal@oksana_spawn.script:monsters_attack:1009                                                    (Взрослый снорк           )  @  -1.7730, -16.3450,  68.8480
 57: monster     snork_normal@oksana_spawn.script:monsters_attack:1010                                                    (Взрослый снорк           )  @   1.0204, -14.1820,  29.6100
 58: monster     m_poltergeist_dd@oksana_spawn.script:monsters_attack:1011                                                (Ползучий пиро-полтергейст)  @  28.6050, -10.8140,   4.5620
 59: monster     snork_normal@oksana_spawn.script:monsters_attack:1012                                                    (Взрослый снорк           )  @   1.2230, -14.1820,  29.6330
 60: monster     scelet_strong@oksana_spawn.script:monsters_attack:1013                                                   (Зомби-скелет             )  @ -11.0450,   4.1840,  -8.9530
 61: monster     scelet_strong@oksana_spawn.script:monsters_attack:1014                                                   (Зомби-скелет             )  @  40.9370,  -6.2850, -17.8600
 62: monster     scelet_strong@oksana_spawn.script:monsters_attack:1015                                                   (Зомби-скелет             )  @  16.6490,  -6.6200, -18.3810
 63: monster     pri_monolith_ambush_exo1@oksana_spawn.script:monolith_attack:1019                                        (Фриц                     )  @  -7.8600,   9.3120,  -6.8160
 64: monster     tank_spawn_zasada1@oksana_spawn.script:monolith_attack:1021                                              (GENERATE_NAME_stalker    )  @  30.4340,   4.1890,  11.4500
 65: monster     tank_spawn_zasada3@oksana_spawn.script:monolith_attack:1023                                              (GENERATE_NAME_stalker    )  @  -7.9520,   4.2060,  -0.7530
 66: monster     tank_spawn_zasada4@oksana_spawn.script:monolith_attack:1024                                              (GENERATE_NAME_stalker    )  @  18.8330,   4.1890, -13.0960
 67: monster     tank_spawn_zasada5@oksana_spawn.script:monolith_attack:1025                                              (GENERATE_NAME_stalker    )  @  -1.7790,   9.3210,  -3.8070
 68: monster     tank_spawn_zasada6@oksana_spawn.script:monolith_attack:1026                                              (GENERATE_NAME_stalker    )  @  -1.0220,   9.3120,   0.9310
 69: monster     tank_spawn_zasada@oksana_spawn.script:monolith_attack:1027                                               (GENERATE_NAME_stalker    )  @  -8.1030,   9.1740,   0.2920
 70: monster     tank_spawn_zasada1@oksana_spawn.script:monolith_attack:1028                                              (GENERATE_NAME_stalker    )  @   6.2820,   4.1870,  -0.0106
 71: monster     tank_spawn_zasada2@oksana_spawn.script:monolith_attack:1029                                              (GENERATE_NAME_stalker    )  @  -6.5900,   1.6910, -14.1010
 72: monster     tank_spawn_zasada3@oksana_spawn.script:monolith_attack:1030                                              (GENERATE_NAME_stalker    )  @  -6.6570,  -3.3050, -12.3490
 73: monster     tank_spawn_zasada4@oksana_spawn.script:monolith_attack:1031                                              (GENERATE_NAME_stalker    )  @  -6.7340,  -8.3070, -12.4480
 74: monster     tank_spawn_zasada5@oksana_spawn.script:monolith_attack:1032                                              (GENERATE_NAME_stalker    )  @ -40.6160, -13.0070, -20.5900
 75: monster     tank_spawn_zasada6@oksana_spawn.script:monolith_attack:1033                                              (GENERATE_NAME_stalker    )  @  40.3430,  -6.3590, -32.0940
 76: monster     tank_spawn_zasada@oksana_spawn.script:monolith_attack:1034                                               (GENERATE_NAME_stalker    )  @  -0.3920,   9.3180, -11.1620
 77: monster     tank_spawn_zasada6@oksana_spawn.script:monolith_attack_two:1039                                          (GENERATE_NAME_stalker    )  @  -7.8600,   9.3120,  -6.8160
 78: monster     tank_spawn_zasada@oksana_spawn.script:monolith_attack_two:1040                                           (GENERATE_NAME_stalker    )  @  -0.8980,   4.3530, -11.1380
 79: monster     tank_spawn_zasada1@oksana_spawn.script:monolith_attack_two:1041                                          (GENERATE_NAME_stalker    )  @  30.4340,   4.1890,  11.4500
 80: monster     tank_spawn_zasada2@oksana_spawn.script:monolith_attack_two:1042                                          (GENERATE_NAME_stalker    )  @ -15.8160,   4.1880,   8.2470
 81: monster     tank_spawn_zasada3@oksana_spawn.script:monolith_attack_two:1043                                          (GENERATE_NAME_stalker    )  @  -7.9520,   4.2060,  -0.7530
 82: monster     tank_spawn_zasada4@oksana_spawn.script:monolith_attack_two:1044                                          (GENERATE_NAME_stalker    )  @  18.8330,   4.1890, -13.0960
 83: monster     tank_spawn_zasada5@oksana_spawn.script:monolith_attack_two:1045                                          (GENERATE_NAME_stalker    )  @  -1.7790,   9.3210,  -3.8070
 84: monster     tank_spawn_zasada6@oksana_spawn.script:monolith_attack_two:1046                                          (GENERATE_NAME_stalker    )  @  -1.0220,   9.3120,   0.9310
 85: monster     tank_spawn_zasada@oksana_spawn.script:monolith_attack_two:1047                                           (GENERATE_NAME_stalker    )  @  -8.1030,   9.1740,   0.2920
 86: monster     tank_spawn_zasada1@oksana_spawn.script:monolith_attack_two:1048                                          (GENERATE_NAME_stalker    )  @   6.2820,   4.1870,  -0.0106
 87: monster     tank_spawn_zasada2@oksana_spawn.script:monolith_attack_two:1049                                          (GENERATE_NAME_stalker    )  @  -6.5900,   1.6910, -14.1010
 88: monster     tank_spawn_zasada3@oksana_spawn.script:monolith_attack_two:1050                                          (GENERATE_NAME_stalker    )  @  -6.6570,  -3.3050, -12.3490
 89: monster     tank_spawn_zasada4@oksana_spawn.script:monolith_attack_two:1051                                          (GENERATE_NAME_stalker    )  @  -6.7340,  -8.3070, -12.4480
 90: monster     tank_spawn_zasada5@oksana_spawn.script:monolith_attack_two:1052                                          (GENERATE_NAME_stalker    )  @ -40.6160, -13.0070, -20.5900
 91: monster     tank_spawn_zasada6@oksana_spawn.script:monolith_attack_two:1053                                          (GENERATE_NAME_stalker    )  @  40.3430,  -6.3590, -32.0940
 92: monster     tank_spawn_zasada@oksana_spawn.script:monolith_attack_two:1054                                           (GENERATE_NAME_stalker    )  @  -0.3920,   9.3180, -11.1620
 93: monster     tank_spawn_zasada@oksana_spawn.script:ambush_laboratory_cool:1422                                        (GENERATE_NAME_stalker    )  @  30.4340,   4.1890,  11.4500
 94: monster     tank_spawn_zasada1@oksana_spawn.script:ambush_laboratory_cool:1423                                       (GENERATE_NAME_stalker    )  @ -15.8160,   4.1880,   8.2470
 95: monster     tank_spawn_zasada2@oksana_spawn.script:ambush_laboratory_cool:1424                                       (GENERATE_NAME_stalker    )  @  -7.9520,   4.2060,  -0.7530
 96: monster     tank_spawn_zasada3@oksana_spawn.script:ambush_laboratory_cool:1425                                       (GENERATE_NAME_stalker    )  @  18.8330,   4.1890, -13.0960
 97: monster     tank_spawn_zasada4@oksana_spawn.script:ambush_laboratory_cool:1426                                       (GENERATE_NAME_stalker    )  @  -8.1030,   9.1740,   0.2920
 98: monster     tank_spawn_zasada5@oksana_spawn.script:ambush_laboratory_cool:1427                                       (GENERATE_NAME_stalker    )  @  -5.1610,   4.1900,   7.4800
 99: monster     tank_spawn_zasada6@oksana_spawn.script:ambush_laboratory_cool:1428                                       (GENERATE_NAME_stalker    )  @   6.2820,   4.1870,  -0.0106
100: monster     white_mackintosh_1@oksana_spawn.script:raincoat_laboratory_all:2386                                      (GENERATE_NAME_bandit     )  @   1.4760,   9.3110,  -3.8160
101: monster     white_mackintosh_1@oksana_spawn.script:raincoat_laboratory_all:2387                                      (GENERATE_NAME_bandit     )  @  -1.9300,   9.3210,  -3.6730
102: monster     white_mackintosh_1@oksana_spawn.script:raincoat_laboratory_all:2388                                      (GENERATE_NAME_bandit     )  @   0.0160,   9.3180, -10.9770
103: monster     white_mackintosh_1@oksana_spawn.script:raincoat_laboratory_all:2389                                      (GENERATE_NAME_bandit     )  @  -9.1220,   9.3110,  -2.7000
104: monster     white_mackintosh_1@oksana_spawn.script:raincoat_laboratory_all:2390                                      (GENERATE_NAME_bandit     )  @ -15.7040,   4.1880,   1.7460
105: monster     ecolog_ozerskiy@oksana_spawn.script:raincoat_laboratory_all:2395                                         (Озеров                   )  @  23.8520,  -6.6180, -17.5040
106: human       ds_bandit_respawn_33@oksana_spawn.script:publicity_material_2:4707                                       (sim_bandit_veteran       )  @  17.4360, -12.4260,  20.4890
107: monster     burer_strong@snp_spawn.script:spawn_n_inventory_box_x181_1:867                                           (Матёрый бюрер            )  @  -5.9277, -10.8151,  -4.3734
108: monster     burer_strong@snp_spawn.script:spawn_n_inventory_box_x181_1:868                                           (Матёрый бюрер            )  @  27.9990, -10.8167, -15.0069
109: monster     m_poltergeist_normal_tele@snp_spawn.script:spawn_n_inventory_box_x181_1:869                              (Пси-полтергейст          )  @  27.9990, -10.8167, -15.0069
110: monster     m_poltergeist_normal_tele@snp_spawn.script:spawn_n_inventory_box_x181_1:870                              (Пси-полтергейст          )  @  -5.9277, -10.8151,  -4.3734
111: monster     bloodsucker_strong@xr_effects.script:spawn_krovosos3:2173                                                (Матёрый кровосос         )  @  -2.0032, -10.8160,  26.3331
112: monster     bloodsucker_strong@xr_effects.script:spawn_krovosos4:2176                                                (Матёрый кровосос         )  @  -2.2438, -10.8180,  10.5692
113: monster     bloodsucker_strong@xr_effects.script:spawn_krovosos5:2179                                                (Матёрый кровосос         )  @  -2.6274, -10.8185,  -1.5016
114: monster     bloodsucker_strong@xr_effects.script:spawn_krovosos6:2182                                                (Матёрый кровосос         )  @  -6.3397, -10.8141,  -5.9057
115: monster     leshyi_bandit1@grom_poison.script:clear_yd_poison:145                                                    (specnaz_bandit_veteran   )  @  -4.2750, -12.0030, -14.6270
116: monster     leshyi_bandit2@grom_poison.script:clear_yd_poison:146                                                    (specnaz_bandit_veteran   )  @  22.4840, -10.8280,  -2.3550
117: monster     leshyi_bandit3@grom_poison.script:clear_yd_poison:147                                                    (sim_atp_bandit_master    )  @  -0.8870,   4.1890, -14.7120
118: monster     leshyi_bandit4@grom_poison.script:clear_yd_poison:148                                                    (sim_bandit_veteran       )  @  -8.1090,   4.1890,  -8.9750
119: monster     leshyi_bandit5@grom_poison.script:clear_yd_poison:149                                                    (specnaz_bandit_veteran   )  @   5.6550,   4.2020,  -7.9570
120: monster     leshyi_bandit1@grom_poison.script:clear_yd_poison:150                                                    (specnaz_bandit_veteran   )  @  -4.1630, -10.8090,  -7.1220
121: monster     bw_killers1@mina_partizan.script:partizan_mercenary:28                                                   (Наёмник из Блэквотер     )  @  -8.7666,   9.3265,  -8.0097
122: monster     bw_killers2@mina_partizan.script:partizan_mercenary:29                                                   (Наёмник из Блэквотер     )  @   0.7853,   9.4392,  -2.4773
123: monster     bw_killers2@mina_partizan.script:partizan_mercenary:30                                                   (Наёмник из Блэквотер     )  @  -1.8799,   9.3178,  -7.4826
124: human       partizan_npc@mina_partizan.script:poisk_partizan_spawn:55                                                (Партизан                 )  @  19.0526, -16.3432,  77.3306
125: monster     bw_sniper@mina_partizan.script:mina_partizan_bw:201                                                      (Снайпер из Блэквотер     )  @  12.1328, -16.3452,  35.4917
