  0: item        Criminal note 18                                                     note_zone_18      ("НС - Глючный ёж"                  ) @ -140.9782,   -6.1931,  -64.0362
  1: manual item pushka_fotik@arhara_dialog.script:spawn_pushka_fot:5732              pushka_fotik      (Изоморф                            ) @   29.4700,    4.6540,   25.7130
  2: manual item izomorf_kristal@arhara_dialog.script:spawn_izomorf_krist:5737        izomorf_kristal   (Изоморф-кристалл                   ) @  219.8590,   -7.3290,  -94.6060
  3: manual item bomba_mina_b@arhara_dialog.script:spawn_mina_forest:5758             bomba_mina_b      (Усиленная противопехотная мина     ) @  200.1020,    4.9370,  117.7630
  4: manual item bomba_mina_n@arhara_dialog.script:spawn_mina_forest:5760             bomba_mina_n      (Противопехотная мина               ) @  202.7810,    4.6770,  117.2180
  5: manual item af_blood_green@braad_test.script:bitvakl_spawn:916                   af_blood_green    ("Изумрудная Кровь камня"           ) @  220.3611,  -11.2673, -101.5219
  6: manual item kaktus_izomorf@oksana_spawn.script:birth_isomorphs:126               kaktus_izomorf    (Изоморф                            ) @  203.5260,    0.0010,  -81.4030
  7: manual item mushroom@oksana_spawn.script:on_mushrooms:417                        mushroom          (Мухомор                            ) @  -78.6840,    2.3400,  -52.9140
  8: manual item mushroom@oksana_spawn.script:on_mushrooms:418                        mushroom          (Мухомор                            ) @  -80.5360,    2.2830,  -33.4370
  9: manual item mushroom@oksana_spawn.script:on_mushrooms:419                        mushroom          (Мухомор                            ) @  -53.3660,    3.1980,  -85.2530
 10: manual item mushroom@oksana_spawn.script:on_mushrooms:420                        mushroom          (Мухомор                            ) @  -36.8470,    3.8880,  -89.6580
 11: manual item mushroom@oksana_spawn.script:on_mushrooms:421                        mushroom          (Мухомор                            ) @  -70.9110,    0.4040, -149.2060
 12: manual item mushroom@oksana_spawn.script:on_mushrooms:422                        mushroom          (Мухомор                            ) @  -60.1180,    0.9920, -211.5810
 13: manual item mushroom@oksana_spawn.script:on_mushrooms:423                        mushroom          (Мухомор                            ) @  -15.7020,   -3.9950, -233.0650
 14: manual item mushroom@oksana_spawn.script:on_mushrooms:424                        mushroom          (Мухомор                            ) @  -41.3170,   -0.9160, -263.8880
 15: manual item mushroom@oksana_spawn.script:on_mushrooms:425                        mushroom          (Мухомор                            ) @   42.3890,   -1.2940, -276.3940
 16: manual item mushroom@oksana_spawn.script:on_mushrooms:426                        mushroom          (Мухомор                            ) @   47.4180,   -1.3110, -319.5670
 17: manual item mushroom@oksana_spawn.script:on_mushrooms:427                        mushroom          (Мухомор                            ) @   -4.4650,   -2.3520, -267.9800
 18: manual item mushroom@oksana_spawn.script:on_mushrooms:428                        mushroom          (Мухомор                            ) @    2.7490,   -2.3950, -287.9800
 19: manual item mushroom@oksana_spawn.script:on_mushrooms:429                        mushroom          (Мухомор                            ) @   17.7750,   -0.2220, -195.5150
 20: manual item mushroom@oksana_spawn.script:on_mushrooms:430                        mushroom          (Мухомор                            ) @  -80.8780,   -0.1350, -226.0480
 21: manual item mushroom@oksana_spawn.script:on_mushrooms:431                        mushroom          (Мухомор                            ) @  -88.0490,    0.8180, -244.4280
 22: manual item mushroom@oksana_spawn.script:on_mushrooms:432                        mushroom          (Мухомор                            ) @  -75.5630,    0.2100, -246.1770
 23: manual item mushroom@oksana_spawn.script:on_mushrooms:433                        mushroom          (Мухомор                            ) @  -42.0420,   -0.8670, -264.1770
 24: manual item mushroom@oksana_spawn.script:on_mushrooms:434                        mushroom          (Мухомор                            ) @   79.6070,   -1.2420, -303.6120
 25: manual item mushroom@oksana_spawn.script:on_mushrooms:435                        mushroom          (Мухомор                            ) @   72.7470,   -0.4150, -264.3710
 26: manual item mushroom@oksana_spawn.script:on_mushrooms:436                        mushroom          (Мухомор                            ) @    9.4470,    2.4320, -164.7680
 27: manual item blank_sheet_14@oksana_spawn.script:tunnel_burer:893                  blank_sheet_14    (Записка Журналиста                 ) @ -154.6170,    3.0780,  -28.2940
 28: manual item diary_unlucky@oksana_spawn.script:setup_monsters:2256                diary_unlucky     (Дневник невезучего                 ) @  -78.7940,   -4.9140, -166.4540
 29: manual item snp_note13@snp.script:581:652                                        snp_note13        (Лист дневника неизвестного сталкера) @ -200.1006,   -0.7592,  -77.4725
 30: manual item snp_note10@snp_spawn.script:tele_kl1:233                             snp_note10        (Лист дневника неизвестного сталкера) @ -230.4181,   13.8255, -220.9403
 31: manual item lesnik_detonator@ohota_vypolzen.script:ohota_vypolzen_start:4        lesnik_detonator  (Детонатор NK-100                   ) @ -103.9440,    0.5450,   22.6750
 32: item        red_ammo_vog-25                                                      ammo_vog-25       (Заряд ВОГ-25                       ) @ -125.0185,   -5.1919,  -45.6548
 33: item        red_ammo_vog-25_0000                                                 ammo_vog-25       (Заряд ВОГ-25                       ) @ -125.4384,   -5.1919,  -45.6294
 34: item        red_ammo_vog-25_0001                                                 ammo_vog-25       (Заряд ВОГ-25                       ) @ -125.2409,   -5.1919,  -45.7014
 35: LevelWarper red_forest_to_warlab_shape_0                                                                                                 @  197.5420,    5.9545,  119.4391 1.10000002384186 x 1.10000002384186
 36: LevelWarper red_level_changer_to_atp_shape_0                                                                                             @  -12.4279,    4.9075, -436.3752 65.7874069213867 x 6.42880010604858
 37: LevelWarper red_level_changer_to_limansk_shape_0                                                                                         @ -180.2112,    2.2382, -285.3680 11.4562005996704 x 4.75201177597046
 38: LevelWarper red_level_changer_to_puzir_shape_0                                                                                           @  265.0978,    0.0335, -313.1484 3.03579998016357 x 14.1212005615234
 39: LevelWarper red_level_changer_to_puzir_shape_1                                                                                           @  265.0978,    0.0335, -313.1484 3.4695999622345 x 3.07640290260315
 40: LevelWarper red_level_changer_to_puzir_shape_2                                                                                           @  265.0978,    0.0335, -313.1484 3.46959900856018 x 3.07640290260315
 41: LevelWarper red_level_changer_to_radar_shape_0                                                                                           @  -45.1907,    3.5700,  103.2486 44.560001373291 x 5.74480104446411
 42: item        red_medkit                                                           medkit            (Medkit                             ) @ -120.3773,    1.9804, -258.0580
 43: item        red_medkit_0000                                                      medkit            (Medkit                             ) @ -120.2962,    1.9444, -258.5664
 44: item        red_medkit_0001                                                      medkit            (Medkit                             ) @ -120.2982,    1.9444, -258.7578
 45: item        red_medkit_0003                                                      medkit            (Medkit                             ) @ -120.3201,    1.9444, -258.1802
 46: item        mushroom_forest1                                                     mushroom          (Мухомор                            ) @  -27.0223,   -0.8796, -294.6263
 47: item        mushroom_forest10                                                    mushroom          (Мухомор                            ) @  194.8108,   -1.7354,   62.4528
 48: item        mushroom_forest11                                                    mushroom          (Мухомор                            ) @  218.4797,   -0.3593,   34.3628
 49: item        mushroom_forest2                                                     mushroom          (Мухомор                            ) @   17.7549,    1.2998,  -98.8253
 50: item        mushroom_forest3                                                     mushroom          (Мухомор                            ) @   -4.8398,    0.2346, -155.3056
 51: item        mushroom_forest4                                                     mushroom          (Мухомор                            ) @    7.5248,   -2.5262, -265.6032
 52: item        mushroom_forest5                                                     mushroom          (Мухомор                            ) @  100.3727,   -1.5568, -290.5061
 53: item        mushroom_forest6                                                     mushroom          (Мухомор                            ) @  177.6167,   -1.3044, -290.0134
 54: item        mushroom_forest7                                                     mushroom          (Мухомор                            ) @  179.4318,   -1.1611,  -46.8760
 55: item        mushroom_forest8                                                     mushroom          (Мухомор                            ) @  193.8414,   -4.7335,   10.9854
 56: item        mushroom_forest9                                                     mushroom          (Мухомор                            ) @  215.8037,    1.2875,  102.9057
 57: ArtKiller   restrictor_art_buluz_red_shape_0                                                                                             @  -17.2800,    1.5200,  -90.6900 40.0499992370605 x 40.0499992370605
