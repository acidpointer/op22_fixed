  0: item        Criminal note 9                                                          note_zone_9              ("ТЧ - Пустынный Орёл"     ) @    7.6398,   -0.0003,  225.1405
  1: lost page   document page 0_0                                                        zonedocs_1               (Часть документов          ) @   15.5375,    0.2518,  228.4200
  2: lost page   document page 0_1                                                        zonedocs_2               (Часть документов          ) @  -97.8785,   15.6067,  127.9611
  3: lost page   document page 0_2                                                        zonedocs_3               (Часть документов          ) @  -99.0322,   -0.0009,   67.9054
  4: lost page   document page 0_3                                                        zonedocs_4               (Часть документов          ) @   95.6219,    0.2762, -160.8324
  5: lost page   document page 0_4                                                        zonedocs_5               (Часть документов          ) @   29.6934,    0.7621,    5.1000
  6: lost page   document page 0_5                                                        zonedocs_6               (Часть документов          ) @  206.3706,   10.1484,  -78.8086
  7: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:249                     vorona_egg               (Воронье яйцо              ) @  -34.2580,    1.2290,   61.5520
  8: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:250                     vorona_egg               (Воронье яйцо              ) @  -34.2580,    1.2290,   61.5520
  9: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:251                     vorona_egg               (Воронье яйцо              ) @  -34.2580,    1.2290,   61.5520
 10: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:252                     vorona_egg               (Воронье яйцо              ) @  -34.2580,    1.2290,   61.5520
 11: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:253                     vorona_egg               (Воронье яйцо              ) @    4.9790,    1.4580,  117.2210
 12: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:254                     vorona_egg               (Воронье яйцо              ) @    4.9790,    1.4580,  117.2210
 13: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:255                     vorona_egg               (Воронье яйцо              ) @    4.9790,    1.4580,  117.2210
 14: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:256                     vorona_egg               (Воронье яйцо              ) @  110.1410,    2.5090,  131.0460
 15: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:257                     vorona_egg               (Воронье яйцо              ) @  110.1410,    2.5090,  131.0460
 16: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:258                     vorona_egg               (Воронье яйцо              ) @  110.1410,    2.5090,  131.0460
 17: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:259                     vorona_egg               (Воронье яйцо              ) @ -136.7710,   -0.1840, -148.6630
 18: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:260                     vorona_egg               (Воронье яйцо              ) @ -136.7710,   -0.1840, -148.6630
 19: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:261                     vorona_egg               (Воронье яйцо              ) @ -136.7710,   -0.1840, -148.6630
 20: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:262                     vorona_egg               (Воронье яйцо              ) @ -163.0500,    2.0870, -161.0240
 21: manual item vorona_egg@arhara_dialog.script:vorona_egg_spawn:263                     vorona_egg               (Воронье яйцо              ) @ -163.0500,    2.0870, -161.0240
 22: manual item af_vyvert_green@arhara_dialog.script:spawn_vyvert_green:406              af_vyvert_green          ("Изумрудный выверт"       ) @    5.3790,   -0.0000,  222.3740
 23: manual item wpn_pkm_m1@arhara_dialog.script:nu_nakonez_to:540                        wpn_pkm_m1               (ПКМ Зулуса                ) @  214.2220,    1.0270,  223.9930
 24: manual item ammo_7.62x54r@arhara_dialog.script:nu_nakonez_to:541                     ammo_7.62x54r            (Короб с патронами 7.62x54R) @  214.2520,    1.0000,  223.4300
 25: manual item ammo_7.62x54r@arhara_dialog.script:nu_nakonez_to:542                     ammo_7.62x54r            (Короб с патронами 7.62x54R) @  214.2520,    1.0000,  223.4300
 26: manual item af_armor_3@arhara_dialog.script:nu_nakonez_to:543                        af_armor_3               ("Панцирь"                 ) @  214.7150,    0.3330,  221.2310
 27: manual item af_night_star@buusty_dialog.script:himik_prygorshnya_spawn:1619          af_night_star            ("Ночная звезда"           ) @  189.0818,    2.5856,   27.8518
 28: manual item af_vyvert@buusty_dialog.script:otday_kartech:1695                        af_vyvert                ("Выверт"                  ) @  169.0985,   -2.1592,   33.8484
 29: manual item af_medusa_green@sak.script:add_medusa_green:308                          af_medusa_green          ("Изумрудная медуза"       ) @    5.3799,   -0.0006,  222.3745
 30: manual item part_krovosos_jaw@bad_day.script:bad_day_spawn:13                        part_krovosos_jaw        (Щупальца кровососа        ) @  -99.8431,   13.8921, -199.6874
 31: manual item conserva@bad_day.script:bad_day_spawn:14                                 conserva                 (Conserva                  ) @ -102.0434,   14.2738, -198.5267
 32: manual item kolbasa@bad_day.script:bad_day_spawn:15                                  kolbasa                  (Kolbasa                   ) @ -101.3591,   14.2497, -198.8221
 33: manual item bread@bad_day.script:bad_day_spawn:16                                    bread                    (Bread                     ) @ -102.2536,   14.2770, -198.1236
 34: manual item vodka@bad_day.script:bad_day_spawn:17                                    vodka                    (Vodka                     ) @ -104.8033,   12.6910, -205.5401
 35: manual item vodka@bad_day.script:bad_day_spawn:18                                    vodka                    (Vodka                     ) @ -104.8033,   12.6910, -205.5401
 36: manual item vodka@bad_day.script:bad_day_spawn:19                                    vodka                    (Vodka                     ) @ -104.9033,   12.9910, -205.4401
 37: manual item vodka@bad_day.script:bad_day_spawn:20                                    vodka                    (Vodka                     ) @ -104.9033,   12.9910, -205.4401
 38: manual item vodka@bad_day.script:bad_day_spawn:21                                    vodka                    (Vodka                     ) @ -104.7033,   13.2910, -205.4401
 39: manual item vodka@bad_day.script:bad_day_spawn:22                                    vodka                    (Vodka                     ) @ -104.7033,   13.2910, -205.3401
 40: manual item vodka@bad_day.script:bad_day_spawn:23                                    vodka                    (Vodka                     ) @ -105.0033,   13.5910, -205.3401
 41: manual item vodka@bad_day.script:bad_day_spawn:24                                    vodka                    (Vodka                     ) @ -105.0033,   13.5910, -205.6401
 42: manual item vodka@bad_day.script:bad_day_spawn:25                                    vodka                    (Vodka                     ) @ -105.1033,   13.7910, -205.6401
 43: manual item vodka@bad_day.script:bad_day_spawn:26                                    vodka                    (Vodka                     ) @ -105.1033,   13.7910, -205.7401
 44: manual item vodka@bad_day.script:bad_day_spawn:27                                    vodka                    (Vodka                     ) @ -105.1033,   13.7910, -205.7401
 45: manual item vodka@bad_day.script:bad_day_spawn:28                                    vodka                    (Vodka                     ) @ -105.2033,   13.9910, -205.8401
 46: manual item vodka@bad_day.script:bad_day_spawn:29                                    vodka                    (Vodka                     ) @ -105.2033,   13.9910, -205.8401
 47: manual item af_medusa@krest_boar.script:bandit_krest_spawn:43                        af_medusa                ("Медуза"                  ) @   -5.4260,   10.9139,  -15.9207
 48: manual item kuznec_koloda@krest_boar.script:bandit_krest_spawn:45                    kuznec_koloda            (Игральная карта           ) @  109.2201,   -0.0027,  -17.1727
 49: manual item af_vyvert@krest_boar.script:bandit_krest_spawn:47                        af_vyvert                ("Выверт"                  ) @  127.9066,    1.2924,   44.3044
 50: manual item af_gravi@krest_boar.script:bandit_krest_spawn:49                         af_gravi                 ("Грави"                   ) @  195.5836,   -3.1657,   73.5328
 51: manual item book_xabarych@shaxter_kniga.script:book_xabarych_spawn:3                 book_xabarych            (Книга Шахтёра             ) @ -218.9180,    0.0260, -223.3270
 52: manual item kuznec_koloda@shaxter_kniga.script:zasada_puzir_spawn:42                 kuznec_koloda            (Игральная карта           ) @ -168.4715,    4.8234, -177.7759
 53: manual item af_cristall_flower@shaxter_kniga.script:zasada_puzir_spawn:44            af_cristall_flower       ("Каменный цветок"         ) @ -228.4215,   -0.0014, -161.3564
 54: manual item af_fireball@shaxter_kniga.script:zasada_puzir_spawn:46                   af_fireball              ("Огненный шар"            ) @ -186.8350,    2.1430, -164.8051
 55: item        puzir_arhara_art3                                                        af_blood                 ("Кровь камня"             ) @ -192.7623,   -0.0120,   52.9719
 56: item        puzir_arhara_art8                                                        af_blood                 ("Кровь камня"             ) @ -210.8066,   -0.1267,  167.9372
 57: item        puzir_arhara_art9                                                        af_blood                 ("Кровь камня"             ) @  -59.7213,   -0.1529,   41.9222
 58: item        puzir_arhara_art2                                                        af_dummy_glassbeads      ("Мамины бусы"             ) @ -107.7098,   -7.7024,  198.6222
 59: item        puzir_arhara_art10                                                       af_electra_flash         ("Вспышка"                 ) @ -166.8434,   -8.9500,  -32.7590
 60: item        puzir_arhara_art4                                                        af_electra_flash         ("Вспышка"                 ) @ -158.9339,   -3.8714, -106.4284
 61: item        puzir_arhara_art5                                                        af_electra_moonlight     ("Лунный свет"             ) @ -170.3223,    3.9173, -173.4525
 62: item        puzir_arhara_art6                                                        af_fuzz_kolobok          ("Колобок"                 ) @   -6.2099,    2.9770,   50.3327
 63: item        puzir_arhara_art7                                                        af_fuzz_kolobok          ("Колобок"                 ) @  205.0550,    7.8447, -103.5708
 64: item        puzir_arhara_art1                                                        af_medusa                ("Медуза"                  ) @ -131.8964,   -5.9304,  216.9949
 65: item        puzir_arhara_art11                                                       af_medusa                ("Медуза"                  ) @  -65.8951,  -10.1587,    6.7722
 66: item        puzir_arhara_art12                                                       af_medusa                ("Медуза"                  ) @ -105.1744,   -7.1397,  194.9337
 67: LevelWarper to_svalka_level_changer_from_puzir_shape_0                                                                                     @ -225.4590,    0.0000,  223.8789 10.7217903137207 x 10.7217903137207
 68: LevelWarper to_td_level_changer_from_puzir_shape_0                                                                                         @  209.4829,    8.1733, -104.0959 2.29999995231628 x 2.29999995231628
