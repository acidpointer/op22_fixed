# This program is distributed under the terms of WTFPL version 2 (see LICENSE attached)

import sys
import os
import re
import collections
import json
import argparse

lang = None
strings = None

def getstring (b):
  if isinstance (b, bytes):
    if b == b'':
      return ''
    tr = strings[lang].get (b.decode (defenc).lower (), b.decode (defenc))
  else:
    if b == '':
      return ''
    tr = strings[lang].get (b.lower (), b)
  return tr

def trader_id_match (a, b):
  if a == b:
    return True
  if a.startswith ("sedoy") and b.startswith ("sedoy"):
    return True
  return False

def get_trader_name (trader):
  return " / ".join ([x["string"] for x in trader["instances"]])

def get_trader_inst_name (inst):
  return inst["string"]

def get_trader_inst_loc (inst):
  return inst["level"]

def get_trader_inst_name_loc (inst):
  return f"{get_trader_inst_name (inst)} ({get_trader_inst_loc (inst)})"

def get_trader_name (trader):
  return " / ".join (list (set ([get_trader_inst_name (x) for x in trader["instances"]])))

def get_trader_name_loc (trader):
  locs = set ([get_trader_inst_loc (x) for x in trader["instances"]])
  result = []
  for i, loc in enumerate (locs):
    if i > 0:
      result.append (f" / ")
    result.append (f"{loc} (")
    i2 = 0
    names = set ()
    for inst in trader["instances"]:
      if inst["level"] == loc and inst["string"] not in names:
        if i2 > 0:
          result.append (f", ")
        result.append (inst["string"])
        names.add (inst["string"])
        i2 += 1
    result.append (f")")
  return "".join (result)

def print_traders (trader_list):
  ls = 1
  lid = 1
  for t in trader_list:
    trader_name = get_trader_name_loc (t)
    ls = max (ls, len (trader_name))
    lid = max (lid, len (t["trade_ltx"]))

  def trader_sort_key (t):
    return get_trader_name (t)

  trader_list.sort (key=trader_sort_key)
  for t in trader_list:
    trader_name = get_trader_name_loc (t)
    print (f"{trader_name:<{ls}} {t['trade_ltx']:<{lid}}")

def main ():
  parser = argparse.ArgumentParser (description="Query trade data")
  parser.add_argument ("--language-file", metavar="FILENAME", default="strings.json", help="Full path to strings.json (produced by collect_strings.py; defaults to %(default)s)")
  parser.add_argument ("--trade-file", metavar="FILENAME", default="trade.json", help="Full path to trade.json (produced by collect_trade.py; defaults to %(default)s)")
  parser.add_argument ("--lang", metavar="LANGUAGE", default="rus", help="Language id to use for queries to strings.json (defaults to %(default)s)")
  command_group = parser.add_mutually_exclusive_group (required=True)
  command_group.add_argument ("--buy", action="store", help="See where to buy an item (by name)")
  command_group.add_argument ("--buy-id", action="store", help="See where to buy an item (by ID)")
  command_group.add_argument ("--sell", action="store", help="See where to sell an item (by name)")
  command_group.add_argument ("--sell-id", action="store", help="See where to sell an item (by ID)")
  command_group.add_argument ("--best-seller-id", action="store", help="See which items a particular trader (by ID) sells at the lowest rates")
  command_group.add_argument ("--best-buyer-id", action="store", help="See which items a particular trader (by ID) buys at the highest rates")
  command_group.add_argument ("--best-seller", action="store", help="See which items a particular trader (by name) sells at the lowest rates")
  command_group.add_argument ("--best-buyer", action="store", help="See which items a particular trader (by name) buys at the highest rates")
  command_group.add_argument ("--list-traders", action="store_true", default=False, help="Lists all the trader IDs and names")

  parser.add_argument ("--exact", action="store_true", default=False, help="Disables partial matches (i.e. 'foo' will not match 'foobar' anymore; by default matches are partial)")
  parser.add_argument ("--nocase", action="store_true", default=False, help="Disables case-sensitive matching (i.e. 'FOO' will match 'foo'; by default matches are case-sensitive)")
  parser.add_argument ("--invert", action="store_true", default=False, help="Invert the best-* commands to see the prices that are NOT the best")

  parser.add_argument ("--encoding", action="store", default="cp866", help="Use the specified encoding in stdout (for example, 'utf-8', 'cp1251' or 'cp866'). Default is %(default)s.")

  args = parser.parse_args ()

  sys.stdout.reconfigure (encoding=args.encoding)

  global lang
  lang = args.lang

  global strings
  with open (args.language_file, 'rb') as sf:
    data = sf.read ().decode ('utf-8')
    strings = json.loads (data)

  with open (args.trade_file, "rb") as sf:
    data = sf.read ().decode ("utf-8")
  tj = json.loads (data)
  traders = tj["traders"]
  traders_by_id = tj["traders_by_id"]
  traded_items = tj["traded_items"]
  traded_items_by_id = tj["traded_items_by_id"]

  traders_by_name = collections.OrderedDict ()
  traded_items_by_name = collections.OrderedDict ()
  for k, v in tj["traders_by_name"].items ():
    traders_by_name[getstring (k)] = v
  for k, v in tj["traded_items_by_id"].items ():
    traded_items_by_name[getstring (traded_items[v]["string_key"])] = v
    traded_items[v]["string"] = getstring (traded_items[v]["string_key"])
  for t in traders:
    for i in t["instances"]:
      i["string"] = getstring (i["string_key"])
      i["level"] = getstring (i["level_caption_key"])

  offers = []

  exact = args.exact
  nocase = args.nocase

  search_string = args.buy or args.buy_id or args.sell or args.sell_id or args.best_buyer or args.best_seller or args.best_seller_id or args.best_buyer_id
  if search_string:
    search_string_lower = search_string.lower ()

  if args.buy or args.sell:
    to_query = traded_items_by_name
  elif args.buy_id or args.sell_id:
    to_query = traded_items_by_id
  elif args.best_seller or args.best_buyer:
    to_query = traders_by_name
  elif args.best_seller_id or args.best_buyer_id:
    to_query = traders_by_id

  if args.buy or args.buy_id or args.sell or args.sell_id or args.best_seller or args.best_buyer or args.best_seller_id or args.best_buyer_id:
    if args.exact and not args.nocase:
      result = [v for k, v in to_query.items () if search_string == k]
    elif args.exact and args.nocase:
      result = [v for k, v in to_query.items () if search_string_lower == k.lower ()]
    elif not args.exact and args.nocase:
      result = [v for k, v in to_query.items () if search_string_lower in k.lower ()]
    else:
      result = [v for k, v in to_query.items () if search_string in k]

  if args.best_seller or args.best_buyer:
    if len (result) == 0:
      print (f"Trader name \"{search_string}\" is not found")
      return
    elif len (result) > 1:
      print (f"Trader name \"{search_string}\" matches multiple traders:")
      print_traders (result)
      print ("Be more specific")
      return
  elif args.best_seller_id or args.best_buyer_id:
    if len (result) == 0:
      print (f"Trader id {search_string} is not found")
      return
    elif len (result) > 1:
      print (f"Trader id {search_string} matches multiple traders:")
      print_traders (result)
      print ("Be more specific")
      return
  if (args.buy or args.sell) and len (result) == 0:
    if args.exact:
      print (f"No item named \"{search_string}\"")
    else:
      print (f"No items have \"{search_string}\" in their name")
    return

  if (args.buy_id or args.sell_id) and len (result) == 0:
    if args.exact:
      print (f"No item with id \"{search_string}\"")
    else:
      print (f"No items have \"{search_string}\" in their id")
    return

  if args.buy or args.buy_id:
    for item_index in result:
      item_index_s = str (item_index)
      traded_item = traded_items[item_index]
      traders_sell_and_stock = set (traded_item["traders_can_sell"]).intersection (set (traded_item["traders_stock"]))
      for trader_index in traders_sell_and_stock:
        trader = traders[trader_index]
        trader_offer_price = []
        trader_offer_stock = []
        for sell_condition in trader["sell_conditions"]:
          if item_index_s in sell_condition["items"]:
            trader_offer_price.append ({"sell_condition_id": sell_condition["id"], "item_index": item_index, "prices": sell_condition["items"][item_index_s]})
        for supply in trader["supplies"]:
          if item_index_s in supply["items"]:
            trader_offer_stock.append ({"supply_id": supply["id"], "item_index": item_index, "supply": supply["items"][item_index_s]})
        if len (trader_offer_price) > 0 and len (trader_offer_stock) > 0:
          offers.append ({"item_index": item_index, "trader_index": trader_index, "trader_offer_stock": trader_offer_stock, "trader_offer_price": trader_offer_price})

    def sell_offers_key (o):
      i = [0, 0]
      for p in o["trader_offer_price"]:
        if p["prices"][1] > i[1]:
          i = p["prices"]
      return -i[0] + -i[1]

    offers.sort (key=sell_offers_key, reverse=True)

    one_item = len (result) == 1 and len (offers) > 0

    if one_item:
      item_id = traded_items[offers[0]["trader_offer_price"][0]["item_index"]]["id"]
      item_string = traded_items[offers[0]["trader_offer_price"][0]["item_index"]]["string"]
      print (f"{item_string} ({item_id}) sold at:")
    for o in offers:
      trader = traders[o["trader_index"]]
      item_id = traded_items[o["item_index"]]["id"]
      item_string = traded_items[o["item_index"]]["string"]
      cost = traded_items[o["item_index"]]["cost"]
      if not one_item:
        print (f"{item_string} ({item_id}) sold at {get_trader_name_loc (trader)} [{trader['trade_ltx']}]:")
      else:
        print (f"{get_trader_name_loc (trader)} [{trader['trade_ltx']}]:")
      for s in o["trader_offer_stock"]:
        sell_cond = s["supply_id"]
        sell_quantity = s["supply"][0]
        sell_prob = s["supply"][1]
        print (f"  stock x{sell_quantity:<2} @ {int (sell_prob * 100):>3}% {sell_cond}")
      for p in o["trader_offer_price"]:
        lower_mul = float (p["prices"][0])
        upper_mul = float (p["prices"][1])
        lower_rur = int (lower_mul * cost)
        upper_rur = int (upper_mul * cost)
        print (f"  price {lower_rur:<6} - {upper_rur:>6} (x{lower_mul:<04.02f} - x{upper_mul:<04.02f}) {p['sell_condition_id']}")
  elif args.sell or args.sell_id:
    for item_index in result:
      item_index_s = str (item_index)
      traded_item = traded_items[item_index]
      for trader_index in traded_item["traders_buy"]:
        trader = traders[trader_index]
        trader_offer_price = []
        for buy_condition in trader["buy_conditions"]:
          if item_index_s in buy_condition["items"]:
            trader_offer_price.append ({"buy_condition_id": buy_condition["id"], "item_index": item_index, "prices": buy_condition["items"][item_index_s]})
        if len (trader_offer_price) > 0:
          offers.append ({"item_index": item_index, "trader_index": trader_index, "trader_offer_price": trader_offer_price})

    def buy_offers_key (o):
      i = [0, 0]
      for p in o["trader_offer_price"]:
        if p["prices"][1] > i[1]:
          i = p["prices"]
      return i[0] + i[1]

    offers.sort (key=buy_offers_key, reverse=True)

    one_item = args.exact and len (offers) > 0 or not args.exact and len (offers) == 1

    if one_item:
      item_id = traded_items[offers[0]["trader_offer_price"][0]["item_index"]]["id"]
      item_string = traded_items[offers[0]["trader_offer_price"][0]["item_index"]]["string"]
      print (f"{item_string} ({item_id}) bought at:")

    for o in offers:
      trader = traders[o["trader_index"]]
      item_id = traded_items[o["item_index"]]["id"]
      item_string = traded_items[o["item_index"]]["string"]
      cost = traded_items[o["item_index"]]["cost"]
      if not one_item:
        print (f"{item_string} ({item_id}) bought at {get_trader_name_loc (trader)} [{trader['trade_ltx']}]:")
      else:
        print (f"{get_trader_name_loc (trader)} [{trader['trade_ltx']}]:")
      for p in o["trader_offer_price"]:
        lower_mul = float (p["prices"][0])
        upper_mul = float (p["prices"][1])
        lower_rur = int (lower_mul * cost)
        upper_rur = int (upper_mul * cost)
        print (f"  {lower_rur:<6} - {upper_rur:>6} (x{lower_mul:<04.02f} - x{upper_mul:<04.02f}) {p['buy_condition_id']}")
  elif args.list_traders:
    print_traders (traders)
    return
  elif args.best_seller or args.best_seller_id:
    trader_index = result[0]
    trader = traders [trader_index]

    def prices_sort_key (p):
      return p["best_price"][0]

    output = []

    for item_index, item in enumerate (traded_items):
      item_traders = set (item["traders_can_sell"]).intersection (set (item["traders_stock"]))
      if trader_index not in item_traders:
        continue

      item_index_s = str (item_index)
      prices = []

      for other_trader_index in item_traders:
        other_trader = traders[other_trader_index]

        best_cond_price = None
        best_cond = None

        for sell_condition in other_trader["sell_conditions"]:
          item_condition = sell_condition["items"].get (item_index_s, None)
          if item_condition is None:
            continue
          if best_cond_price is None or best_cond_price[1] > item_condition[1]:
            best_cond_price = item_condition
            best_cond = sell_condition
        if best_cond_price:
          prices.append ({"trader_index": other_trader_index, "best_sell_condition_id": sell_condition['id'], "best_price": best_cond_price})

      prices.sort (key=prices_sort_key)

      if len (prices) > 0:
        for p in [x for x in prices if x["trader_index"] == trader_index]:
          this_traders_price = p["best_price"]
          break
        lowest_price = prices[0]
        tie_length = 1
        trader_is_best = lowest_price["trader_index"] == trader_index
        for p in prices[1:]:
          if p["best_price"][0] != lowest_price["best_price"][0]:
            break
          tie_length += 1
          if p["trader_index"] == trader_index:
            trader_is_best = True
            lowest_price = p
        if (trader_is_best and not args.invert) or (not trader_is_best and args.invert):
          lower_mul = this_traders_price[0]
          lower_rur = int (lower_mul * item["cost"])
          s = [f"{item['string']} ({item['id']}) @ {lower_rur} (x{lower_mul}) on {lowest_price['best_sell_condition_id']}"]
          if tie_length > 1 or args.invert:
            if not args.invert:
              s += [f" (tied with "]
            else:
              tie_length = max (tie_length, 1)
              s += [f" (better at "]
          first = True
          n = 0
          trader_name = get_trader_name (trader)
          for p in prices[:tie_length]:
            other_trader = traders[p["trader_index"]]
            other_trader_name = get_trader_name (other_trader)
            if other_trader_name != trader_name and (not args.invert or p["best_price"][0] != this_traders_price[0]):
              if not first:
                s += [", "]
              first = False
              s += [f"{get_trader_name (other_trader)}"]
              n += 1
          if n > 0:
            s += [")"]
          if not args.invert or n > 0:
            output.append (["".join (s), lower_mul])
    output.sort (key=lambda x: x[1])
    for o in output:
      print (o[0])
  elif args.best_buyer or args.best_buyer_id:
    trader_index = result[0]
    trader = traders [trader_index]

    def prices_sort_buy_key (p):
      return p["best_price"][1]

    def all_prices_sort_buy_key (p):
      return p["best_price"]["best_price"][1]

    all_prices = []

    for item_index, item in enumerate (traded_items):
      if trader_index not in item["traders_buy"]:
        continue

      item_index_s = str (item_index)
      prices = []

      for other_trader_index in item["traders_buy"]:
        other_trader = traders[other_trader_index]

        best_cond_price = None
        best_cond = None

        for buy_condition in other_trader["buy_conditions"]:
          item_condition = buy_condition["items"].get (item_index_s, None)
          if item_condition is None:
            continue
          if best_cond_price is None or best_cond_price[2] < item_condition[2]:
            best_cond_price = item_condition
            best_cond = buy_condition
        if best_cond_price:
          prices.append ({"trader_index": other_trader_index, "best_buy_condition_id": best_cond['id'], "best_price": best_cond_price})

      prices.sort (key=prices_sort_buy_key, reverse=True)

      if len (prices) > 0:
        for p in [x for x in prices if x["trader_index"] == trader_index]:
          this_traders_price = p
        highest_price = prices[0]
        tie_length = 1
        trader_is_best = highest_price["trader_index"] == trader_index
        for p in prices[1:]:
          if p["best_price"][1] != highest_price["best_price"][1]:
            break
          tie_length += 1
          if p["trader_index"] == trader_index:
            trader_is_best = True
            highest_price = p
            
        if (trader_is_best and not args.invert) or (not trader_is_best and args.invert):
          all_prices.append ({"item": item, "trader_index": trader_index, "best_price": this_traders_price})
          for p in prices[:tie_length]:
            if p["trader_index"] != trader_index:
              all_prices[-1].setdefault ("other_traders", []).append (p["trader_index"])

    all_prices.sort (key=all_prices_sort_buy_key, reverse=True)

    trader_name = get_trader_name (trader)
    for p in all_prices:
      cost = p['item']['cost']
      mul = p['best_price']['best_price'][1]
      highest_price = int (mul * cost)
      print (f"{p['item']['string']} ({p['item']['id']}) @ {highest_price} (x{mul}) on {p['best_price']['best_buy_condition_id']}", end="")
      first = True
      n = 0
      for other_trader_index in p.get ("other_traders", []):
        other_trader = traders[other_trader_index]
        other_trader_name = get_trader_name (other_trader)
        if other_trader_name == trader_name:
          continue
        if not first:
          print (", ", end="")
        else:
          if not args.invert:
            print (f" (tied with ", end="")
          else:
            print (f" (better at ", end="")
        first = False
        print (f"{get_trader_name (other_trader)}", end="")
        n += 1
      if n > 0:
        print (")", end="")
      print ("")

if __name__ == "__main__":
  main ()
