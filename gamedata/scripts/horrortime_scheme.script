class("horrortime_evaluator")(property_evaluator)

function horrortime_evaluator:__init(st)
	super(nil, "horrortime_evaluator")
	self.st = st
end

function horrortime_evaluator:evaluate()
	if self.st == nil or not self.st.enabled then
		return false
	end
	local npc = self.object
	if xr_wounded.is_wounded(npc) then
		return false
	end
	if horrortime.is_npc_ignore_HT(npc) then
		return false
	end
	local HT_state = get_value("HT_state", 0)
	return (HT_state == 1 or HT_state == 2)
end

class("horrortime_action")(action_base)

function horrortime_action:__init(st)
	super(nil, "horrortime_action")
	self.st = st
end

local HT_npc_states = { "hide_s_left", "hide_s_right", "hide_no_wpn", "threat", "hide_na" }

function horrortime_action:initialize()
	action_base.initialize(self)
	self:set_npc_HT_state()
	stop_play_sound(self.object)
	xr_sound.set_sound(self.object, "")
end

function horrortime_action:execute()
	action_base.execute(self)
	if self.nextupd < time_global() then
		self:set_npc_HT_state()
	end
end

function horrortime_action:set_npc_HT_state()
	self.nextupd = time_global() + 5000
	if not self.prefang then
		self.prefang = math.random(0, 360)
	end
	local preflook
	preflook, self.prefang = blowout_scheme.find_open_dir(self.object, self.prefang, 30)
	self.state = table.random(HT_npc_states)
	state_mgr.set_state(self.object, self.state, nil, nil, { look_position = preflook })
end

local npcCommunityIgnore = {
	["zombied"] = true,
	["trader"] = true,
	["arena_enemy"] = true,
}

local evid_horrortime = stalker_ids.property_script + 5431
local actid_horrortime = stalker_ids.action_script + 5432

function add_to_binder(object, char_ini, scheme, section, storage)
	local monolith = object:character_community() == "monolith"
	local ignored = npcCommunityIgnore[object:character_community()] or horrortime.is_npc_ignore_HT(object) or monolith
	local manager = object:motivation_action_manager()
	if ignored then
		manager:add_evaluator(evid_horrortime, property_evaluator_const(false))
	else
		manager:add_evaluator(evid_horrortime, horrortime_evaluator(storage))
		local action = horrortime_action(storage)
		action:add_precondition(world_property(stalker_ids.property_alive, true))
		action:add_precondition(world_property(stalker_ids.property_danger, false))
		action:add_precondition(world_property(evid_horrortime, true))
		action:add_effect(world_property(evid_horrortime, false))
		manager:add_action(actid_horrortime, action)

		action = manager:action(xr_actions_id.alife)
		action:add_precondition(world_property(evid_horrortime, false))
		-- action = manager:action (xr_actions_id.action_danger_planner)
		-- action:add_precondition(world_property(evid_horrortime, true))
	end
end

function disable_scheme(npc, scheme)
	if npc and scheme and npc:id() and db.storage[npc:id()] then
		local st = db.storage[npc:id()][scheme]
		if st then
			st.enabled = false
		end
	end
end

function set_scheme(npc, ini, scheme, section)
	local st = xr_logic.assign_storage_and_bind(npc, ini, scheme, section)
	st.ini = ini
	st.enabled = true
end
