///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Dynamic Shaders 2.0 Alpha Default Preset - By Meltac, Ketxxx
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Feature Enable/Disable: Comment a feature (Add the //) to disable, or uncomment (Remove the //) to enable.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef SSH_Mount
#define SSH_Mount

// Basic settings
//#define SSH					                            // Enable/disable Sun shafts 
#define SSH_NUM_SAMPLES float(160) 		          // Quality. Higher is better. 32 is accebtable, 128 high quality (254 = max)
#define SSH_EXPOSURES   float2(0.13,0.05) // 0.20          // Maximum intensity of sun shafts
#define SSH_EXPOSUREIN float(3) 		            // Correction factor for indoor sun shafts. Use only with SSH_INDOORDETECTION enabled

// Advanced settings
#define SSH_WEIGHTS float2(0.35,0.35) 	// 0.01			      // Relative "weight" of the light rays
#define SSH_DENSITYS float2(0.9,0.6) 	// 0.9		        // Density of the light rays
#define SSH_DECAYS float2(1.003,0.95) // 1.012		        // Fall-Off (Decay) of the light rays
#define SSH_BLUR float(2.0)			                // Blur effect and sets radius of blur applied to sunshafts
#define SSH_COMPHI float(8.0)			              // Sets exposure compression of high-luminance rays ("low-pass"). Brighter rays will be toned down with less contrast 
#define SSH_COMPHISOFT float(0.5)  // 0.25		            // Softness of low-pass exposure compression. 0=no compression, 0.25 very soft, 0.75 rather hard, 1=limiting
#define SSH_COMPLO float(5.0)		// 2.0	              // Sets exposure compression of low-luminance rays ("high-pass"). Less bright rays will be pronounced with less contrast 
#define SSH_COMPLOSOFT float(0.50) 		            // Softness of high-pass exposure compression. 0=no compression, 0.25 very soft, 0.75 rather hard, 1=limiting
#define SSH_SUNHEIGHTHI  float(1.0) 		        // Sun shafts are only visible if the sun is not higher than this value
#define SSH_SUNHEIGHTMAX float(0.800) 	        // Sun shafts are most intense at this sun height (intensity peak)
#define SSH_SUNHEIGHTLO  float(0.100) 	        // Sun Shafts are only visible if the sun is not lower than this value
#define SSH_SUNCOL	float(0.3)					// Amount of sun color as declared in weather settings adopted by sun shafts. 
#define SSH_NATURAL								// Use "natural" sampling (=default). If disabled, a more "technical" sampling is used. Disable to lessen flickering / film effect, but adjust SSH_EXPOSURES and SSH_SUNCOL, too.

// Additional sun streaks
//#define SSH_STREAKS float(0.75)						// Enables/disables sun streaks in addition to sun shafts and sets their intensity. Lowers FPS a little.
#define SSH_STREAKSWIDTH float(4.5) //(2.475)			// Width of the primary streaks effect.
#define SSH_STREAKSWIDTH2 float(1.0) // (2.075)		// Width of the secondary streaks effect.
#define SSH_STREAKSLUMDIFF float(0.0)					// Difference of brightness for streaks to appear. Better leave untouched if unaware of effect.
#define SSH_STREAKSROTX float(-10.0)	// 10			// Rotation speed of streaks around the X-axis when turning head.
#define SSH_STREAKSROTY float(-10.0)	// 10			// Rotation speed of streaks around the Y-axis when turning head.
#define SSH_STREAKSSUNNEARMIN float (0.025)			// Distance to the sun at which the streaks start to get rendered (not noticeable below this value, getting brighter above).
#define SSH_STREAKSSUNNEARMAX float (0.15)			// Distance to the sun at which the streaks have reached their full intensity / brightness.
#define SSH_STREAKSSUNDISTMIN float (0.25)			// Distance to the sun at which the streaks start to get less insense. Decrease if experiencing glitches.
#define SSH_STREAKSSUNDISTMAX float (0.5)			// Distance to the sun at which the streaks are no more visible. Decrease if experiencing glitches.
#define SSH_STREAKSSUNCOL	float(0.7)				// Amount of sun color as declared in weather settings adopted by sun streaks. 
#define SSH_STREAKSMINALPHA float(0.0)				// Alpha level above which streaks will be visible.

//#define SSH_DUST						// Should add some dust but not working yet. Leave disabled.

// Pro setting - Change only if experiencing issues!
#define SSH_FARPLANE float(180.0)
#define SHH_ADP float(0.75)			                // Alternative ray marching algorithm for distance-based sampling. 0=none/classic ,1=adaptive
#define SSH_ADPKERNEL float(0.37)		            // Distance kernel factor when adaptive sampling is enabled (see line above)
#define SSH_MINLUMDIFF  float(0.0)		            // Required to prevent shader artifacts on walls and objects. Prone to creating "sky blob" artifacts. See also SSH_MINLUMCOF.
#define SSH_MINLUMCOF float(0.5)					// Amount of prevention of shader artifacts on walls and objects. 0.0=none, 1.0=max. Decrease if experiencing "sky blob" artifacts. See SSH_MINLUMDIFF.
#define SSH_MINALPHA float(0.1) 		            // Required to prevent ghosting in the sky and indoor sunshafts. Best 0.10, max 0.45
#define SSH_MAXALPHA float(0.95) 		            // Required to prevent ghosting in the sky and indoor sunshafts. Best 0.95, min 0.65
#define SSH_MINALPHA2 float(0.0)		            // Like MINALPHA but for INDOORDETECTION2
#define SSH_MAXALPHA2 float(2.0)		            // Like MAXALPHA but for INDOORDETECTION2
#define SSH_MAXALPHAPART1 float(0.00001)	      // Required to prevent indoor sunshafts 
#define SSH_MAXALPHAPART2 float(0.000011)	      // Required to prevent indoor sunshafts 
#define SSH_MAXALPHARAY float(0.79)		          // Required to prevent indoor sunshafts
#define SSH_MINSOLID float(0.30)		            // Required to prevent indoor sunshafts
#define SSH_MINSKIES float(0.0001)		          // Required to prevent indoor sunshafts
#define SSH_MINRAY float(4.f) //float(0.000001)	// Required to prevent indoor sunshafts
//#define SSH_MINDEPTH float(5.0)

// Alpha Correction - Prevention of dark smear effects
#define SSH_ALPHA float(0.2)					// Enables/disables alpha correction, and, if enabled, sets the alpha amount at which correction comes in.
#define SSH_ALPHACORR float(0.4)				// Amount of alpha correction, if enabled.

// Fading - according to sun distance, height, exposure - Also: Prevention of "film" effects
#define SSH_SUNFADEMINX float(2.0) // 2.0	      // Angle between sun and view direction at which the light rays start to get less insense
#define SSH_SUNFADEMAXX float(3.0) // 3.0	      // Angle between sun and view direction at which the light rays are no more visible
#define SSH_SUNNEARMIN float (0.0)			// Distance to the sun at which the light rays start to get rendered (not noticeable below this value, getting brighter above).
#define SSH_SUNNEARMAX float (1.0)			// Distance to the sun at which the light rays have reached their full intensity / brightness.
#define SSH_SUNDISTMIN float (4.5) // 4.5			// Distance to the sun at which the light rays start to get less insense. Decrease if experiencing flickering / film effect.
#define SSH_SUNDISTMAX float (5.5) // 5.5			// Distance to the sun at which the light rays are no more visible. Decrease if experiencing flickering / film effect.
#define SSH_SUNEXPOMIN float (0.0)
#define SSH_SUNEXPOMAX float (1.0)

// Sun position correction - probably not anymore needed - left for legacy purposes
#define SSH_POSCORRX   float(0.15)		          // Sun position correction factor at x-axis
#define SSH_POSCORRYHI float(0.2)		            // Sun position correction factor at y-axis above screen center
#define SSH_POSCORRYLO float(1.2)		            // Sun position correction factor at y-axis below screen center
#define SSH_POSHEIGHT float(0.573)		          // Sun reference height for sun height adjustments
#define SSH_POSHEIGHTCORRX float(-1.5)		      // Sun height adjustment/correction at x-axis
#define SSH_POSHEIGHTCORRY float(0.5)		        // Sun height adjustment/correction at y-axis
//#define SSH_DEBUGSUNHEIGHT			              // Enables/disables sun height debug mode

// Indoor detection - Prevention of rays beam through solid walls
//#define SSH_INDOORDETECTION			              // Image-based indoor/particles detection (experimental)
#define SSH_INDOORDETECTION2			              // Enables/disables another indoor detection (experimental)
#define SSH_PREVENT float(0.5)			            // Heuristic indoor/particle godrays prevention. 0=show godrays, 1=no godrays when possibly indoors. 0.5=half intensity when possibly indoors.
#define SSH_SOLIDCHECK				// Enables/disables solid walls check for indoor/particle sun shafts prevention (see line above). Causes minor FPS lost but normally enhances outdoor sunshafts.
//#define SSH_ADVLIGHT					// Enables/disables advanced lighting control. Eats a few FPS with minor visual improvement if enabled.

#endif