#ifndef COMPAT_CONFIG
#define COMPAT_CONFIG

//////////////////////////////////////////////////////////////
// Legagy Sun Shafts (by Sky4ce)
//////////////////////////////////////////////////////////////
// WARNING !!!
// This section exist only for compatibility reasons!
// DO NOT EDIT THIS PARAMETERS!!!
//
// ������!
// ��� ������ ������������ ������������� ��� ������������� �������� ����� �����.
// ������������� �� ������������� �������������!
// ��������� �������� ��������� ��������� � ������ ������!
//
//Basic options:
//#define SUN_SHAFTS                // Enables Sun Shafts (god rays)
                                    // Only noticable on Full Dynamic Lighting, looks very nice but causes poor performance on some computers.
                                    // Also makes the environment much brighter and natural.
#define RAY_SAMPLES int(16)         // Amount of sun ray samples. The higher this is, the better the quality and the lower your framerate. 12
#define SS_INTENSITY float(2.0)     // intensity of sun shafts. 1.1
#define SUN_SHAFTS_FIX              // Comment or Uncomment to enable or disable fix for far/near boundary.
                                    // Adjust SS_FAR_CONTRAST to hide the seam (it will never be perfect in all possible lighting). (By Daemonjax)

#define SS_NEAR_CONTRAST float(6)   // Amount of contrast during sunray calculation (near). 6
#define SS_FAR_CONTRAST float(5.35) // Amount of contrast during sunray calculation (far).
                                    // Adjust to hide near/fear seam when sun shafts are enabled and SUN_SHAFTS_FIX is enabled.
#define SS_CORRECTION float(-3.0)   // Meltac: Sunshaft direction Y axis correction

#endif // COMPAT_CONFIG