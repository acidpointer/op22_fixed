// Original author: Meltac, Sky4ce, K.D., others
// Configured by: acidpointer aka 0xD6
// Espe�ially for Cumulative Pack 2.2

// Default config is optimized for performance and picture,
// you can do nothing with configuration =)

#ifndef _SHADERS_CONFIG_H
#define _SHADERS_CONFIG_H

//////////////////////////////////////////////////////////////
//Screen Space Ambient Occlusion (SSAO)
//////////////////////////////////////////////////////////////

#define SSAO                // Enables Screen Space Ambient Occlusion  # Looks good if you are stuck on Object Dynamic Lighting
#define SSAO_QUALITY 2      // Amount of SSAO samples.
                            // Use 2 for low quality and 3 for highst quality.
                            // 0 and 1 are only valid when using SSAO_COP.
                            // The only noticable difference is the framerate :P
//#define SSAO_COP          // Enable SSAO ported from Call of Pripyat (but without jittering). Probably the best SSAO algorithm so far.

#define SSAO_PASSES int(7) // ���������� �������� ����� SSAO. �� ������ ������ ������� 6 ��������.
         // Use a minimum of two (since actual number of passes is one less than
         // this number)

#define SAO_DENSITY int(1024) // higher values increase sao definitition while reducing its size.
            // No performance cost.  As you increase the number of passes,
            // you'll want to increase density

//#define SSAO_HIGH_QUALITY     // increases the number of unique samples for each pass to 12

// Options only for SSAO_LEGACY:
#define SSAO_TEX_CONT           // Increase texture contrast for diffuse lighting
                                // This line makes SSAO much darker. If it is too dark add a // to the start of the line or increase the value on the line below.
#define CONT_COEF float(0.3)    // Brightness increase amount
//Only for testing.
#define FADE_DIST float(4)
#define FADE_COEF float(0.2)

// Options only for SSAO_COP:
#define SSAO_KERNEL_SIZE 90         // How big SSAO samples are. 70 is default. 150 for greater effect.
#define SSAO_NOISE_TILE_FACTOR 2    // Not sure if this works or not, honestly. Seems to be used to determine how big the jitter sample is.

// Options only for implementation by daemonjax (=if no other SSAO implementation has been enabled above)
#define SSAO_PASSES int(5)          // Amount of SSAO sample passes.  Each pass takes 6 samples. Use a minimum of two (since actual number of passes is one less than this number).  6
//#define SSAO_HIGH_QUALITY         // Increases the number of unique samples for each pass to 12.
#define SAO_DENSITY int(1024)       // Higher values increase sao definitition while reducing its size.
                                    // No performance cost.  As you increase the number of passes,  you'll want to increase density. 1024
////For testing:
//#define SSAO_NOLOOP               // performs only 1 pass.  Same as SSAO_PASSES = 2, except it also removes the forloop in the code.  If defined, SSAO_PASSES has no effect.


// Bloom
#define ECB_BLOOM_DIV 0.69h           //0.69h �� ����� �������� �������� ���������� ������
#define ECB_BLOOM_SKYDIV 0.95h        //0.95h �� ����� �������� �������� ���������� ������ (����)
#define ECB_BLOOM_BRIGHTNESS 0.5f     //0.5f ������� �����
#define ECB_SPECULAR 0.61f            //0.61f  ������� ������
#define ECB_LCOMPRESS                 // bloom 2218
#define ECB_AUTOSAT_FACTOR 8.f     //21.25f  ���������� ��������� �������� ��� �������� ������������(�������������)
#define ECB_BRIGHTNESS 1.f        //0.9375f ������� �������� � �����


//////////////////////////////////////////////////////////////
//Parallax Occlusion Mapping
//////////////////////////////////////////////////////////////

//Basic Options:
#define PARALLAX_OCCLUSION              // Enables Parallax Occlusion Mapping  # Makes surfaces look bumpy and 3d at the cost of fps. Worth it
#define MAX_SAMPLES int(40)             // Maximum amount of samples done to a texture.
#define MIN_SAMPLES int(4)              // Minimum amount of samples done to a texture.
#define PARALLAX_OFFSET float(0.015)    // Height of surface per sample.
#define FINAL_INTERSECTION_LOOPS int(5) // Amount of additional samples to increase accuracy.

//Performance Options:
#define PARALLAX_FADE               // Parllax textures fade back to regular normals with distance; increases FPS and fixes anisotropic filtering.
                                    // Disabling this strangely gave me a much higher FPS.
#define START_FADE float(0.0003)    // Distance the fading starts
#define STOP_FADE float(0.0004)     // Distance the fading stops, and the texture returns to just using normals.

//Other Options:
#define CONTRAST_COEF_Q1 float(0.6) //Amount of contrast in calculations.
#define CONTRAST_COEF_Q2 float(1)   //Amount of contrast in calculations.
#define BRIGHTNESS_COEF float(0.25) //Amount of brightness in calculations.

//////////////////////////////////////////////////////////////
//Float32 v2.0
//////////////////////////////////////////////////////////////

#define USE_F32             // Enables Float32 mod. Breaks saturation and contrast filters. # If you are using Panoramic Mod 2.1 FINAL from Argus make sure you have a // at the start of this line
#define USE_F32_FOG         // Enables Float32 style fog. Requires Float32 to be enabled.
#define CT_int  1.25f       // Cook Tolerance of the sun.
#define CT_int_in   1.25f   // Cook Tolerance of indoor lighting.
#define BLOOM_val 0.6h      // Bloom brightness - Increases HDR brightness of the sky
#define HDR_int  10.h       // HDR cutoff - Total HDR brightness

/////////////////////////////////////////////////////////////////////////////////////////////////////
//Anti-Aliasing (does not really work well, use at your own risk; at the moment, non-zoom mode only)
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Basic options:
#define AA                                  // Enables Anti-Aliasing
#define AA_KERNEL float(0.5874)             // PS: 0.5

#define AA_BARRIER_X float(0.900000)        // (0.900000)
#define AA_BARRIER_Y float(0.500000)        // (0.500000)
#define AA_BARRIER_Z float(0.300000)        // (0.000000)

#define AA_WEIGHTS_X float(0.250000)        // (0.250000)
#define AA_WEIGHTS_Y float(0.250000)        // (0.250000)
#define AA_WEIGHTS_Z float(0.000000)        // (0.000000)

//////////////////////////////////////////////////////////////
// Lighting - Shadows - Jittering Options
//////////////////////////////////////////////////////////////

//#define USE_SJITTER         // Uses shadow jittering, a modern method of smoothing out shadow edges. Applies to flashlight, and to world if SUN_FILTER is enabled.
//#define USE_SJITTER_SUN     // Uses shadow jittering for shadows created by the sun/moon, a modern method of smoothing out shadow edges. Highly recommended.
//#define USE_SJITTER_OTHER   // Enables shadow jittering (4 samples) for dynamic lights (e.g. flashlight/campfires).  Recommended.
#define USE_SUNMASK         // Enables sun shadow masking # Occludes sun/moon behind clouds (pretty sure, anyways).

// Shadow resolution:   NOTE:  You will STILL have to use the -smapXXXX switch on your shortcut to the game executable, where XXXX = CUSTOM_SHADOW_RESOLUTION!!!
#define KERNEL float(1.6)                   // IMPORTANT For custom shadow resolution... Minimum recommended values: 1024 = .6, 2048 = 1.2, 4096 = 2.4, etc... Higher values produce softer shadows.
#define XKERNEL float(1.6)                  // Probably should be same as KERNEL!
#define CUSTOM_SHADOW_RESOLUTION int(3096)  // Use a custom shadow size. Default is 1024 (1024x1024). You'll notice a performance hit at 4096.

/////////////////////////////////////////////////////////////
//Motion Blur
//////////////////////////////////////////////////////////////
//Basic options:
#ifndef USE_MBLUR
#define USE_MBLUR               // Enables Motion Blur # This forces Motion Blur on like -mblur. Adjust the blur amount with MBLUR_COEF later in this script.
#endif // USE_MBLUR
#define IMPROVED_MBLUR          // Use distance-depending Motion Blur - Adjust START_DIST and FINAL_DIST to your needs.

#define START_DIST float(1.0)   // Interpolation start distance
#define FINAL_DIST float(300.0) // Interpolation end distance

/////////////////////////////////////////////////////////////
// ����������� ��������
#define USE_TREEWAVE_EN	                    // �������� ������ ����������� ��������
#define TREEWAVE_EN_FACTOR int(5)			// ���� ����������� (integer)

// ������������� � ����� 2218
#define USE_OLDSTYLE_GLOSS			// �������� ������������� � ����� �� ����� 2218
#define OLDSTYLE_GLOSS_FACTOR half (16.0)    // �������������.

/////////////////////////////////////////////////////////////
// ����
#define SW_USE_FOAM                     // �������� "����" ������
#define SW_FOAM_THICKNESS half (0.09)   // (0.07)(0.035) ������� "����"
#define SW_WATER_INTENSITY half (1.0)   // (1.0) ������� ����� ����
#define USE_SOFT_WATER                  // ������ ����(KD98)
#define USE_SOFT_PARTICLES              // ������ ��������(������ � ���� ������ ���������) - �������� �� �������� ��.

/////////////////////////////////////////////////////////////
// ����� �� �����
#define USE_GRASS_WAVE                  // �������� "�����" �� ����� �� �����
#define GRASS_WAVE_POWER float(4.0)     // "�������", ���������� ����
#define GRASS_WAVE_FREQ float(0.7)      // ������� ��������� ����

////////////////////////////////////////////////////////////
// ������������� �������
#define TEX_CONT                          // �������� ������
#define TEX_CONT_COEF float(0.5)          // (0.75) ��� ���� �������� ��� ����� ������������ ����� ��������
#define TEX_CONT_ADD float(0.3)           // (0.0) �������������� ��������� ���������


/////////////////////////////////////////////////////////////
//  Saturation and Contrast
//#define FORCED_SATURATION_FILTER             // Meltac: Forces the above saturation filter in cases in which it does not work.

#define CONTRAST_FILTER                        // enables Contrast Filter, giving a grey like appearance to areas which are "unsafe".
// Basic options:
#define COLOR_SATURATION float(1.1)             //(0.80) ������� ������. 0 - ��� ����� (�����-�����), 1 - ������ ��� (������ ��� ��������?)
#define COLOR_SAT_SUN_COEF float(0.0)           //(0.60) ��� ������ ������ ��������� ��������� �� ������������
#define CONTRAST_FILTER_COEF float(0.025)       // �evel of full screen contrast.	


/////////////////////////////////////////////////////////////
// �������� ��� GodRays
#define USE_SS_FRONT                                   // �������� ��������
//#define SS_FRONT_BLUR                                // �������� ���������� � ���������� ������
//#define SS_FRONT_DUST                                // �������� ���� � ���������(���� ������)
#define SS_FRONT_DENSITY float(0.6)                    // (0.5)1.0 "���������" �����, ��� ������, ��� ��� ������, �� ��� ���� ��������
#define SS_FRONT_SAMPLES int(110)                      // (110) �������� ��������� (integer)
#define SS_FRONT_INTENSITY float(0.6)                  // (0.9)(1.4) ������������� �����
#define SS_FRONT_OUTCOLOR_R float(0.3)                 // (0.3) ���� ��������� ���������� � ���������� ������ R(�������)
#define SS_FRONT_OUTCOLOR_G float(0.3)                 // (0.3) ���� ��������� ���������� � ���������� ������ G(������)
#define SS_FRONT_OUTCOLOR_B float(0.3)                 // (0.3)���� ��������� ���������� � ���������� ������ B(�����)
#define SS_FRONT_BLEND_FACTOR float(0.4)               // (0.4) ������ ���������� � ���������� ������. ��� ������ ��������, ��� ������ "��������" �� �������, �� �������� "����������"
#define SS_FRONT_DUST_SPEED float(1.2)                 // �������� ������ ����
#define SS_FRONT_DUST_INTENSITY float(4.5)             // ������� �������
#define SS_FRONT_DUST_DENSITY float(0.5)               // ��������� ������ ����
#define SS_FRONT_DUST_SIZE float(1.9)                  // ������ �������
#define SS_FRONT_FARPLANE float(180.0)                 // ����� �� ��������

/////////////////////////////////////////////////////////////
// Bloom
#define ECB_BLOOM_DIV 0.53h           // 0.69h (0.53h)      
#define ECB_BLOOM_SKYDIV 0.95h        // 1.15h/0.95h       ()
#define ECB_BLOOM_BRIGHTNESS 0.25f    // 0.5f (0.25f)  
#define ECB_SPECULAR 0.61f             // 0.61f   
#define ECB_LCOMPRESS                 // bloom 2218
#define ECB_AUTOSAT_FACTOR 21.25f     // 21.25f       ()
#define ECB_BRIGHTNESS 0.9375f        // 0.9375f (1.25f)   

/////////////////////////////////////////////////////////////
// Depth Of Field - By Meltac, Sky4ce
//#define DOF_CHECK 1                                     // ����� ������������� ��������
//#define DEPTH_OF_FIELD                                  // Enables Depth Of Field   # Blurs objects in the distance
#define DOF_QUALITY float(12)                           // Amount of DOF samples. Maximum value is 12.
//Distance Options:
#define MINDIST float(15.0)                             //(15.0) ����������� ���������� �� ������. �������� �� ��������� ����� 0.4.  // TWILIGHT KET USES 15
#define MAXDIST float(275)                              // ����������, �� ������� DOF ���������� ���������. Default is 375
#define MAXCOF	float(2.5)                               //(2.5) Blur ����� - if using DDOF, ��� �� ������ ������� ������ ���������� �� DDOF_MAXCOF (I use same value). 2.5 is good.	


// Meltac: Near Field DOF, Zoom DOF, and Dynamic DOF

// Blur algorithm - Classic blur by Sky4ce (faster), or linear gaussian blur by Meltac (slower but smoother/better quality)
#define DOF_CLASSIC                             // Use classic blur algorithm; if disabled, use gaussian blur algorithm (=default).
//#define DOF_CLASSIC_ZOOM                      // Same as above but for zoom mode
#define DOF_CORRECTION float (2.0)              // ����������� ��� ������������ �������� �������� �� ������ � ������������ ���������. �������� �� ��������� - 2.0.
//#define DOF_CRYSIS                            // Enables modified/simplied blur effect ported from Crysis 2. CAUTION: Highly computationally expensive! Default is disabled.

// Oversampling for better quality (not really improving much, but making game startup slower - use at your own risk)
#define DOF_OVERSAMPLING int(1)                 // oversampling ratio. Default is 1.

// Near Field DOF (see also Dynamic DOF, because the effects are interfering!)	
#define MAXNEAR float(0.8)                  // Distance at which Near Field DOF starts. Default is 0.7. Larger values will blur your weapon more, and not only its near part.
#define MINNEAR float(0.0)                  // Distance at which Near Field DOF stops. Default is 0.0
#define MAXCOF_NEAR float(7.0)              // Near Field blur amount. Interferes heavily with DDOF. Default is value 7.0 without, 15.0 with DDOF.

// Zoom DOF
//#define ZOOM_PERIBLUR                           // Enable/disable peripheral blur in zoom/aim mode.
//#define ZOOM_DDOF                               // Enable/disable dynamic DOF in zoom/aim mode.
#define ZOOM_CENTER float2(0.500001f,0.497f)    // (����������� ����� ��������� ������ ��� ���������� DOF ���������������. �������� ��� ��������, ���� ����� ������ ��� ���������������
#define ZOOM_FOV float (67.5)                   // Field of View (FOV) angle at which zoom/aim mode is considered to be active. Do not change this value by default.
#define ZOOM_MINDIST float(15.0)                // ����������� ���������� (������) �� ������ ������, ��� ���������� DOF.
#define ZOOM_MAXDIST float(260.0)               // Distance radius from screen center at which DOF stops rendering (beyond that value - full blur).
#define ZOOM_MAXCOF float(80.0)                 // ������� �������� ������� ������ ��� ���������������.
#define ZOOM_FACTOR float(10.0)                 // ������� �������� ������������� �� ������ � �������. ������� �������� - 10-12..
#define ZOOM_BARRIER float (20.0)               // (DEBUG) ���������� �� ������ ������, �� ������� ���������� ��������, � ������������� ��������.
	
// Dynamic DOF (caution: interferes with near field DOF)
//#define DDOF                                  // Enable/disable Dynamic DOF (blurring coefficient is DDOF_MAXCOF)
#define DDOF_CENTER float2(0.500001f,0.500001f) // ����������� ����� ��������� ������ ��� ������������� ������� DOF. �� ��������� ��� �������� �� ���������..	
#define DDOF_MINDIST_ADD float(1.5)             // ��������� ���������� �����������, �� ���� ���������� ���������� ��������stance, at this distance begins blurring
#define DDOF_MAXDIST_FACTOR float(4.0)          // Mindist * factor = maxdist
#define DDOF_MINDIST float(2.0)                 // Minimum focus distance where DDOF starts rendering. Was 1.5 or 3.5
#define DDOF_MAXDIST float(7.0)                 // Focus distance at which DDOF start to fade/decrease blur.
#define DDOF_MAXBARRIER float(15.0)             // (15.0) �������� ����������, �� ������� DDOF ������������� ���������.
                                                // �� ��������� ����� ��������� ���������� ��� DDOF, ������ ������� (�������������) DOF
#define DDOF_MINBARRIER float (4.0)             // (4.0) Focus distance that makes the upper barrier for near field DDOF.
#define DDOF_MAXCOF	float(5.0)                  // DDOF distance blur factor - good balanced is value 2.0 - 5.0 - see also MAXCOF
#define DDOF_MAXCOF_NEAR float (2.5)            // DDOF near field blur factor
#define DDOF_MAXALPHA float (1.0)               // Used to fix wrong DDOF distance when aiming to the sky. Change only if experiencing issues.

// Debug only - do not use in production environment
//#define DDOF_DEBUG

#include "_compat_config.h"

#endif // _SHADERS_CONFIG_H
