/*  Volumetric Sunlights aka GodRays aka Sunshafts
	made by K.D. (OGSE team)
	used papers:
	1) GPU Gems 3. Volumetric Light Scattering as a Post-Process (Kenny Mitchell)
	2) ���������� ���������� �������� �������������. ����� 2 - �������� ����-��������. (�������� ������� �Nikola Tesla�)
	3) Crysis shaders :)*/

#ifndef	_SUN_SHAFTS_H
	#define _SUN_SHAFTS_H
	#include "_shaders_config.h"
	#include "common.h"

uniform float4 		common_params2;

			// dust samplers
uniform sampler2D 	s_jitter_0;	// ����������� ���, 64x64
uniform sampler2D 	s_jitter_1;	// ����������� ���, 64x64
uniform sampler2D 	s_jitter_5;	// ��� �������� ����������, 1024x1024
		
	float normalize_depth(float depth) {
		return (saturate(depth/SS_FRONT_FARPLANE));
	}
	
	float3 blend_soft_light(float3 a, float3 b) {
		float3 c = 2 * a * b + a * a * (1 - 2 * b);
		float3 d = sqrt(a) * (2 * b - 1) + 2 * a * (1 - b);
		return ( b < 0.5 )? c : d;
	}

	float3 calc_sunshafts(float2 tc, float3 init_color) {  
		#ifdef USE_SS_FRONT
			float3 out_color = float3(0,0,0);
			float sun_dist = SS_FRONT_FARPLANE / (sqrt(1 - L_sun_dir_w.y * L_sun_dir_w.y));
			float4 sun_pos_projected = mul( m_VP, float4( sun_dist * L_sun_dir_w + eye_position, 1 ) );
			float4 sun_pos_screen = convert_to_screen_space(sun_pos_projected)/sun_pos_projected.w;
			float2 sun_vec_screen = sun_pos_screen.xy - tc;

			float angle_cos = dot(-eye_direction, normalize(L_sun_dir_w));
			float ray_fade = saturate(saturate(angle_cos)*(1 - saturate(sun_vec_screen))*saturate(length(sun_vec_screen)*1000));
			float2 init_tc = tc;
			
			float3 pos = tex2D(s_position, tc);
			float dep = pos.z;
		#ifdef SS_FRONT_BLUR
			float mask = (saturate(angle_cos*0.1 + 0.9) + normalize_depth(pos.z) - 0.99999) * (SS_FRONT_DENSITY - 0.1);
			sun_vec_screen *= angle_cos * (SS_FRONT_DENSITY - 0.1) / SS_FRONT_SAMPLES;
		#else
            float mask = (saturate(angle_cos*0.9) + normalize_depth(pos.z) - 0.99999) * SS_FRONT_DENSITY;
			sun_vec_screen *= angle_cos * SS_FRONT_DENSITY / SS_FRONT_SAMPLES;
		#endif
			float depth_accum = 0;
		
			for (float i = 0; i < SS_FRONT_SAMPLES; i++) { 
				tc += sun_vec_screen;
				#ifdef SS_FRONT_BLUR
					float blur_delta = 2 * (.5f/1024.f);	
					out_color += (tex2Dlod(s_image, float4(tc + blur_delta,0,0)) + tex2Dlod(s_image, float4(tc - blur_delta,0,0))) * (1.f/2.f) * SS_FRONT_BLEND_FACTOR;
				#else
					out_color += float3(SS_FRONT_OUTCOLOR_R, SS_FRONT_OUTCOLOR_G, SS_FRONT_OUTCOLOR_B);
				#endif
				depth_accum += saturate(1 - tex2D(s_position, tc).z*1000);
			}			
			out_color *= saturate(depth_accum/SS_FRONT_SAMPLES);
		#ifdef SS_FRONT_DUST
			float dust_size = 8/SS_FRONT_DUST_SIZE;
			float3 jit;
			float2 jtc = init_tc;
			float2 sun_dir_e = L_sun_dir_e.xy;
			sun_dir_e /= sin(common_params2.x);
			sun_dir_e *= common_params2.y;
			jtc.x += sun_dir_e.x;
			jtc.y -= sun_dir_e.y;
			jtc.x = (jtc.x > 1.0)?(jtc.x-1.0):jtc.x;
			jtc.y = (jtc.y < 0.0)?(1.0-jtc.y):jtc.y;
			jit.x = tex2D(s_jitter_0, float2(jtc.x, jtc.y + timers.x*0.01*SS_FRONT_DUST_SPEED)*dust_size).x;
			jit.y = tex2D(s_jitter_1, float2(jtc.x + timers.x*0.01*SS_FRONT_DUST_SPEED, jtc.y)*dust_size).y;
			jit.z = tex2D(s_jitter_5, jtc).x;
			jit.z = saturate(jit.z + SS_FRONT_DUST_DENSITY - 1);
			float3 dust = saturate(float3(jit.x, jit.x, jit.x)*float3(jit.y, jit.y, jit.y)*float3(jit.z, jit.z, jit.z));
			float len = length(dust);
			dust *= SS_FRONT_DUST_INTENSITY;
			dust = lerp(0, dust, (1 - saturate(dep * 0.2)) * (1 - saturate((0.001 - dep)*10000.0)));
			dust += float3(1,1,1);
		#else
			float3 dust = float3(1,1,1);
		#endif
			out_color *= 2 * ray_fade / SS_FRONT_SAMPLES * dust;
		#ifdef SS_FRONT_BLUR
			out_color = init_color + out_color * L_sun_color * ((SS_FRONT_INTENSITY + 0.4)- init_color);
		#else
			out_color = init_color + out_color * L_sun_color * (SS_FRONT_INTENSITY - init_color);
		#endif
			//out_color = blend_soft_light(out_color, L_sun_color * mask * -0.1 + 0.6) - init_color;
			  out_color = blend_soft_light(out_color, L_sun_color * mask * -0.1 + 0.5) - init_color;
			return out_color;
		#else
			return 0;
		#endif
	}  
#endif // _SUN_SHAFTS_H