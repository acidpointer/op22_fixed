#ifndef _CLOUDCONFIG_H
#define _CLOUDCONFIG_H

// note: timers has resolution (sec), where x=1, y=10, z=1/10,
#define CLOUD_TILE0        (0.7f)     //(0.0f)
#define CLOUD_SPEED0       (0.1f)   //(0.033f)скорость одного слоя. Если двигается в обратную сторону - может поставить минус?
#define CLOUD_TILE1        (2.8f)     //(2.5f)
#define CLOUD_SPEED1       (0.05f)   //(0.033f)скорость другого слоя
#define CLOUD_FADE         (0.5)      //(0.5) возможно накл. на туман 

#endif
